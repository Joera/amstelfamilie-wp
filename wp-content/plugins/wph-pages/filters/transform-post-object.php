<?php

function change_template_names($data) {

    if($data['type'] == 'amstelveenlijn' && $data['slug'] == 'homepage-amstelveenlijn') {

        $data['type'] = 'homepage';
        $data['render_environments'] = ['amstelveenlijn'];

    } elseif ($data['slug'] == 'werkzaamheden-en-planning' && $data['type'] == 'amstelveenlijn') {

        $data['type'] = 'werkzaamheden-en-planning';
        $data['render_environments'] = ['amstelveenlijn'];

    } elseif ($data['slug'] == 'werkzaamheden-en-planning' && $data['type'] == 'uithoornlijn') {

            $data['type'] = 'werkzaamheden-en-planning';
            $data['render_environments'] = ['uithoornlijn'];

    } elseif ($data['slug'] == 'over-het-project') {

        $data['type'] = 'over-het-project';
        $data['render_environments'] = ['amstelveenlijn'];

        }

    elseif ($data['slug'] == 'rsvp') {

        $data['type'] = 'rsvp';
        $data['render_environments'] = ['amstelveenlijn'];

    } elseif ($data['type'] == 'uithoornlijn' && $data['slug'] == 'homepage-uithoornlijn') {

        $data['type'] = 'homepage';
        $data['render_environments'] = ['uithoornlijn'];

    } elseif ($data['type'] == 'amsteltram' && $data['slug'] == 'homepage-amsteltram') {

        $data['type'] = 'homepage';
        $data['render_environments'] = ['amsteltram'];

    } elseif ($data['type'] == 'uithoornlijn' && $data['slug'] == 'footer-uithoornlijn') {

        $data['type'] = 'footer';
        $data['render_environments'] = ['uithoornlijn'];

    } elseif ($data['type'] == 'amsteltram' && $data['slug'] == 'footer-amsteltram') {

        $data['type'] = 'footer';
        $data['render_environments'] = ['amsteltram'];

    } elseif ($data['type'] == 'amstelveenlijn' && $data['slug'] == 'footer-amstelveenlijn') {

        $data['type'] = 'footer';
        $data['render_environments'] = ['amstelveenlijn'];

    } elseif ($data['type'] == 'amstelveenlijn') {

        $data['type'] = 'page';
        $data['render_environments'] = ['amstelveenlijn'];

    } elseif ($data['type'] == 'uithoornlijn') {

        $data['type'] = 'page';
        $data['render_environments'] = ['uithoornlijn'];

    } elseif ($data['type'] == 'amsteltram') {

        $data['type'] = 'page';
        $data['render_environments'] = ['amsteltram'];
    }

    PC::debug($data);

    unset($data['_links']);
    return $data;
}

function add_sort_date($data) {

    $data['sort_date'] = $data['date'];

    if($data['updates'] && !empty($data['updates'])) {
        $data['sort_date'] = $data['updates'][0]->date;
    }


    return $data;
}

add_filter( 'transform-post-object', 'change_template_names', 12, 3 );
add_filter( 'transform-post-object', 'add_sort_date', 12, 3 );
