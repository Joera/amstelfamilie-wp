<?php

function check_publish_permissions($post_id) {

    $type = get_post_type($post_id); // get post type
    $new_status = get_post_status($post_id); // get new status
    $old_status = get_post_meta($post_id, 'old_status', true); // get previous status

    PC::debug($type);

    if ($type === 'field-group' || $type == 'acf' || $type == 'feed' || $type == 'subscriber' || $type == 'contact' || $type == 'revision') {
        return false;
    } else if ($type == 'activity' && $new_status == 'draft') {
        // when events are submitted / created // session data is added later / + no need to publish
        return false;
    } else {
        return true;
    }
}

add_filter( 'publish_permissions', 'check_publish_permissions', 12, 3 );
