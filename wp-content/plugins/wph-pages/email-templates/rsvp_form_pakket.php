<?php

include(PLUGIN_FOLDER . 'wph-pages/email-templates/partials/footer_amstelveenlijn.php');
include(PLUGIN_FOLDER . 'wph-pages/email-templates/partials/beeldmerk_amstelveenlijn.php');
include(PLUGIN_FOLDER . 'wph-pages/email-templates/links.php');

$html = '

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>' . $subject . '</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body style="margin: 0; padding: 0; font-family: Arial; font-size: 14px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">

                 <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" width="560" style="margin:0 auto;">
                            <tr>
                                <td>
                                   <img src="https://cms.amsteltram.nl/wp-content/uploads/2020/11/24980-Gemeente-Amsterdam-Uitnodiging-officiele-opening-online-e-mailnieuwsbrief-header_V2-600x168px.jpg" width="580"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="560">
                            
                            <tr>
                                        <td height="0">

                                        </td>

                                    </tr>



                            <!-- Content -->
                            <tr>
                                <td style="padding: 40px 0px 10px 0px;color:#000;">

                                    <div style="font-family: Arial, sans-serif; font-size:20px;line-height:29px;margin:0 0 29px 0;">

                                        Geachte ' . $body->salutation .' ' . $body->last_name . ',<br><br>
                                        Wij nodigen u van harte uit om aanwezig te zijn bij de officiële online opening van lijn 25 en de ingebruikname van de nieuwe 15G trams op zondag 13 december. Een bijzondere mijlpaal!

                                    </div>

                                    <div style="font-family: Arial, sans-serif;font-size:16px;line-height:24px;">

                                        <b>Programma van de opening</b><br><br>

                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="560">
                                            <tr>
                                                    <td width="150" style="vertical-align:top;padding: 8px 0;font-family:Arial,sans-serif;font-size:16px;line-height:24px;">16.00 uur:</td>
                                                    <td style="vertical-align:top;padding: 8px 0;font-family:Arial,sans-serif;font-size:16px;line-height:24px;">Start online livestream</td>
                                            </tr>
                                            <tr>
                                                    <td width="150" style="vertical-align: top;padding: 8px 0;font-family:Arial,sans-serif;font-size:16px;line-height:24px;">16.00 – 16.30 uur: </td>
                                                    <td style="vertical-align: top;padding: 8px 0;font-family:Arial,sans-serif;font-size:16px;line-height:24px;">Warm welkom door onze Razende Reporter en een blik achter de schermen </td>
                                            </tr>
                                            <tr>
                                                    <td width="150" style="vertical-align: top;padding: 8px 0;font-family:Arial,sans-serif;font-size:16px;line-height:24px;">16.30 – 17.00 uur:</td>
                                                    <td style="vertical-align: top;padding: 8px 0;font-family:Arial,sans-serif;font-size:16px;line-height:24px;">Talkshow onder leiding van Amstelvener en presentator Humberto Tan</td>
                                            </tr>
                                            <tr>
                                                    <td width="150" style="vertical-align: top;padding: 8px 0;font-family:Arial,sans-serif;font-size:16px;line-height:24px;">17.00 – 17.25 uur:</td>
                                                    <td style="vertical-align: top;padding: 8px 0;font-family:Arial,sans-serif;font-size:16px;line-height:24px;">Officiële openingshandeling met spectaculaire lichtshow en documentaire over de nieuwe lijn 25</td>
                                            </tr>
                                            <tr>
                                                    <td width="150" style="vertical-align: top;padding: 8px 0;font-family:Arial,sans-serif;font-size:16px;line-height:24px;">17.25 – 17.30 uur:</td>
                                                    <td style="vertical-align: top;padding: 8px 0;font-family:Arial,sans-serif;font-size:16px;line-height:24px;">Afsluiting door Humberto Tan en de Razende Reporter</td>
                                            </tr>
                                            <tr>
                                                    <td width="150" style="vertical-align: top;padding: 8px 0;font-family:Arial,sans-serif;font-size:16px;line-height:24px;">17.30 uur:</td>
                                                    <td style="vertical-align: top;padding: 8px 0;font-family:Arial,sans-serif;font-size:16px;line-height:24px;">Einde bijeenkomst </td>
                                            </tr>

                                        </table><br>

                                       Graag bezorgen wij de speciale <i>lijn 25 is er! celebration box</i> bij u thuis. Als dank voor uw betrokkenheid en inzet én om samen op 13 december online het glas te kunnen heffen.
                                       Zou u uiterlijk donderdag 3 december via deze <a href="https://amstelveenlijn.nl/lijn25iser/rsvp/?token=' . $body->token . '">persoonlijke link</a> uw gegevens aan ons door willen geven? Wij gebruiken deze uitsluitend en eenmalig voor dit doel. Mocht u nog vragen hebben dan kunt u deze stellen via <a style="color:black;text-decoration:none;" href="info@amstelveenlijn.nl">info@amstelveenlijn.nl</a>.<br><br>

                                       We hopen u 13 december online te mogen verwelkomen.<br><br>


                                        Hartelijke groet,<br><br>

                                        <b>Thea de Vries<b><br>
                                        <b>Secretaris directeur</b>

                                    </div>

                                    <img src="https://cms.amsteltram.nl/wp-content/uploads/2020/11/vra-e1605096731284.png"  width="160" style="margin: 0px 0px 0px 0px"/>

                                </td>
                            </tr>
                          
                        </table>
                    </td>
                </tr>



            </table>
        </td>
    </tr>

    <tr>
        <td>
            <table class="footer" align="center" border="0" cellpadding="0" cellspacing="0" width="560">
                <tr>
                    <td>
                        <img src="https://cms.amsteltram.nl/wp-content/uploads/2020/11/24980-Gemeente-Amsterdam-Save-the-Date-e-mailnieuwsbrief_V3-footer-600x60px.jpg" width="580"/>
                    </td>
                </tr>

            </table>
        </td>
    </tr>
</table>
</body>

</html>
';
