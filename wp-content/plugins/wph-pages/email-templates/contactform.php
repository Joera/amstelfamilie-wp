<?php
include(PLUGIN_FOLDER . 'wph-pages/email-templates/partials/footer_' . $env . '.php');
include(PLUGIN_FOLDER . 'wph-pages/email-templates/partials/beeldmerk_' . $env . '.php');

$html = '

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>' . $subject . '</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body style="margin: 0; padding: 0; font-family: Arial; font-size: 14px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
                <tr>
                    <td height="40"></td>
                
                </tr>
                <tr>
                    <td>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="560">
                            
                            ' . $beeldmerk . '

                            <!-- Content -->
                            <tr>
                                <td style="padding: 40px 0px 20px 0px; font-size:13px;line-height:20px;color:#000000">
                                   
                                   ' . $content['name'] . ' met mailadres ' . $content['email'] . ' heeft de volgende vraag of opmerking verstuurd: <br><br>
                                   
                                   ' . $content['message'] . '

                        
                                </td>
                            </tr>
                          
                        </table>
                    </td>
                </tr>
               
  
                ' . $footer . '
                      
                
            </table>
        </td>
    </tr>
</table>
</body>

</html>
';