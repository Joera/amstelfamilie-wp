<?php

include(PLUGIN_FOLDER . 'wph-pages/email-templates/partials/footer_amstelveenlijn.php');
include(PLUGIN_FOLDER . 'wph-pages/email-templates/partials/beeldmerk_amstelveenlijn.php');
include(PLUGIN_FOLDER . 'wph-pages/email-templates/links.php');

$html = '

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>' . $subject . '</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body style="margin: 0; padding: 0; font-family: Arial; font-size: 14px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">

                 <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" width="560" style="margin:0 auto;">
                            <tr>
                                <td>
                                   <img src="https://cms.amsteltram.nl/wp-content/uploads/2020/11/24980-Gemeente-Amsterdam-Save-the-Date-e-mailnieuwsbrief_V3-header-600x168px-1.jpg" width="580"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="480">
                            
                            <tr>
                                        <td height="0">

                                        </td>

                                    </tr>



                            <!-- Content -->
                            <tr>
                                <td style="padding: 40px 0px 10px 0px;color:#000;">

                                    <div style="font-family: Arial, sans-serif; font-size:20px;line-height:29px;margin:0 0 29px 0;">

                                        Geachte ' . $body->salutation .' ' . $body->last_name . ',<br><br>

                                        Na jaren van voorbereidingen op papier, pakweg twee jaar van bouwen en een paar maanden testen, is de klus bijna klaar: het vernieuwen van de Amstelveenlijn! Op zondag 13 december openen we lijn 25 officieel. Ook nemen we de spiksplinternieuwe 15G trams in gebruik. Dat vieren we graag samen met u online.
                                    </div>

                                    <div style="font-family: Arial, sans-serif;font-size:16px;line-height:24px;">

                                       We nodigen u dan ook van harte uit om alvast 13 december van 16.30 tot 17.30 uur in uw agenda te reserveren. U ontvangt later deze maand de uitnodiging hiervoor.<br><br>
                                       We hopen dat u er 13 december online bij kunt zijn.<br><br>


                                        Hartelijke groet,<br><br>

                                        <b>Thea de Vries<b><br>
                                        <b>Secretaris directeur</b>

                                    </div>

                                    <img src="https://cms.amsteltram.nl/wp-content/uploads/2020/11/vra-e1605096731284.png"  width="160" style="margin: 0px 0px 0px 0px"/>

                                </td>
                            </tr>
                          
                        </table>
                    </td>
                </tr>



            </table>
        </td>
    </tr>

    <tr>
        <td>
            <table class="footer" align="center" border="0" cellpadding="0" cellspacing="0" width="560">
                <tr>
                    <td>
                        <img src="https://cms.amsteltram.nl/wp-content/uploads/2020/11/24980-Gemeente-Amsterdam-Save-the-Date-e-mailnieuwsbrief_V3-footer-600x60px.jpg" width="580"/>
                    </td>
                </tr>

            </table>
        </td>
    </tr>
</table>
</body>

</html>
';
