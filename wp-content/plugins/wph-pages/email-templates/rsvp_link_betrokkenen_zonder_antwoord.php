<?php

include(PLUGIN_FOLDER . 'wph-pages/email-templates/partials/footer_amstelveenlijn.php');
include(PLUGIN_FOLDER . 'wph-pages/email-templates/partials/beeldmerk_amstelveenlijn.php');
include(PLUGIN_FOLDER . 'wph-pages/email-templates/links.php');

$html = '

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>' . $subject . '</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body style="margin: 0; padding: 0; font-family: Arial; font-size: 14px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">

                 <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" width="560" style="margin:0 auto;">
                            <tr>
                                <td>
                                   <img src="https://cms.amsteltram.nl/wp-content/uploads/2020/11/24980-Gemeente-Amsterdam-Uitnodiging-officiele-opening-online-e-mailnieuwsbrief-header_V2-600x168px.jpg" width="580"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="560">

                            <tr>
                                        <td height="0">

                                        </td>

                                    </tr>



                            <!-- Content -->
                            <tr>
                                <td style="padding: 40px 0px 10px 0px;color:#000;">

                                    <div style="font-family: Arial, sans-serif; font-size:20px;line-height:29px;margin:0 0 29px 0;">
                                        Geachte ' . $body->salutation .' ' . $body->last_name . ',<br><br>
                                        Op <b>zondag 13 december</b> vindt de <b>opening van lijn 25 online</b> plaats. We hopen uiteraard dat u virtueel met ons mee kijkt.
                                    </div>

                                    <div style="font-family: Arial, sans-serif;font-size:16px;line-height:24px;">

                                        Om de uitzending te bekijken volgt u de volgende instructies:<br>

                                        <ol>
                                            <li>Registreer u voor de exclusieve livestream middels deze link: <a style="color:#f8af00;text-decoration:none;" href="https://primetime.bluejeans.com/a2m/register/pvqyzftu">registratielink</a>
                                            (Krijgt u een foutmelding? Probeer de link dan te openen buiten uw werkomgeving.)</li>
                                            <li>Na uw registratie ontvangt u per e-mail een registratiebevestiging inclusief link naar de livestream.</li>
                                            <li>Op 13 december 2020 kunt u vanaf 16.00 uur terecht op de livestream pagina voor het exclusieve programma voor alle projectbetrokkenen.</li>
                                            <li>Om 16.30 uur start de talkshow onder leiding van Amstelvener en presentator Humberto Tan, gevolgd door de officiële openingshandeling en de documentaire.</li>
                                        </ol>
                                        <br>

                                       <b>Contact</b><br>

                                       Ondervindt u problemen met het bekijken van de livestream of heeft u vragen naar aanleiding van de uitzending?
                                       Mail dan naar: <a style="color:black;"href="mailto:liselotte@carbonevents.nl">liselotte@carbonevents.nl</a>.<br><br>

                                        Veel kijkplezier!<br><br>

                                        <b>Thea de Vries<b><br>
                                        <b>Secretaris directeur</b>

                                    </div>

                                    <img src="https://cms.amsteltram.nl/wp-content/uploads/2020/11/vra-e1605096731284.png"  width="160" style="margin: 0px 0px 0px 0px"/>

                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>



            </table>
        </td>
    </tr>

    <tr>
        <td>
            <table class="footer" align="center" border="0" cellpadding="0" cellspacing="0" width="560">
                <tr>
                    <td>
                        <img src="https://cms.amsteltram.nl/wp-content/uploads/2020/11/24980-Gemeente-Amsterdam-Save-the-Date-e-mailnieuwsbrief_V3-footer-600x60px.jpg" width="580"/>
                    </td>
                </tr>

            </table>
        </td>
    </tr>
</table>
</body>

</html>
';
