<?php

include(PLUGIN_FOLDER . 'wph-pages/email-templates/partials/footer_amstelveenlijn.php');
include(PLUGIN_FOLDER . 'wph-pages/email-templates/partials/beeldmerk_amstelveenlijn.php');
include(PLUGIN_FOLDER . 'wph-pages/email-templates/links.php');

$html = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Zuidas houdt je op de hoogte</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    
    <style type="text/css">
            body {
                font-family: Arial;
                font-size: 14px;
            }

            v\:* { behavior: url(#default#VML); display:inline-block; }


            h2 > span > span {
                position: relative;
                right: 10px;
            }

            .follow-us {
                width: 108px;
                margin: 20px auto 5px auto;
                font-size: 14px;
                line-height: 20px;
                color: #000000;
            }

            .social-icon {
                text-decoration:none;
                margin: 0 3px;
            }
            .footerLinks {
                font-size: 13px;
                line-height: 20px;
                text-align:center;
            }
            .footerLinks p {
                width:500px;
                display:block;
                color: #000;
                margin: 0 auto 7px auto;
            }
            .footerlinks b {
                width:200px;
                text-align:center;
                display:block;
                color: #000;
                font-size: 14px;
                margin:0px auto 0 auto;
            }
            .footerLink {
                display:block;
                width: 240px;
                color:#000;
                margin: 0 auto 20px auto;
            }


    </style>
</head>

<body style="margin: 0; padding: 0; font-family: Arial; font-size: 14px;">
<table border="0" cellpadding="0" cellspacing="0" width="600">
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
                <tr>

                    <td >
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            
                            ' . $beeldmerk . '
                           
                            
                            <!-- Content -->

                              <!--[if (gte mso 9)|(IE)]>

                                  <tr>
                                      <td style="padding: 0px;">
                                          <table align="center" border="0" cellpadding="0" cellspacing="0" width="560">
                                               <tr>
                                                    <td>
                                                        <img src="' . $image_url . '" width="560" />

                                                    </td>
                                               </tr>
                                               <tr>
                                                    <td height="20">
                                                    </td>
                                               </tr>
                                           </table>
                                      </td>
                                  </tr>

                              <![endif]-->

                            <tr>
                                <td style="padding: 0px;">
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="560" height="373" style="margin:0 auto;">
                                         <tr>
                                            <td width="560" valign="bottom" background="' . $image_url . '" style="position:relative;">
                                                <h2 style="width:280px;line-height: 1.5;margin: 0;border-left:10px solid #ffffff;">
                                                    <span class="papa" style="background: #ffffff;color: #000000; padding: 4px 0px 5px 0;">
                                                        <span class="baby" style="position:relative;left:-10px;">' . $post->post_title . '</span>
                                                    </span>
                                                </h2>
                                            </td>
                                        </tr>
                                    </table>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="560" style="margin:0 auto;">
                                        <tr>
                                            <td style="padding: 20px 0px 40px 0px;">
                                                <div style="font-size:13px;line-height:20px;width:440px;color: #000000;">' . $post->post_content . '</div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="560" style="margin:0 auto 30px auto;">
                                        <tr align="center">
                                            <td width="160" align="center" style="margin:0 auto;" >
                                                <span style="text-align:center;border: 1px solid #000000;padding: 5px 20px;">
                                                    <a href="' . $post_url . '" bgcolor="#ffffff" style="color: #000000; text-decoration: none;">Lees verder op de website</a>
                                                </span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
              
                            <!-- Footer -->
                             ' . $footer . '
                        </table>

                    </td>
                    <td width="20">
                        </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>

</html>
';

?>
