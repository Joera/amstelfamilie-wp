<?php

function register_amstelveenlijn() {

    $args = array(
        'label'  => 'Amstelveenlijn',
        'labels' => array(
            'add_new_item' => 'Pagina toevoegen',
            'new_item' => 'Nieuwe pagina',
            'view_item' => 'Bekijk pagina',
            'view_items' => 'Bekijk paginas'
        ),
        'description' => '',
        'public' => true,
        'show_ui' => true,
        'has_archive' => false,
        'show_in_menu' => true,
        'exclude_from_search' => false,
        'capability_type' => 'page',
        'map_meta_cap' => true,
        'hierarchical' => true,
        'rewrite' => array( 'slug' => 'amstelveenlijn', 'with_front' => true ),
        'query_var' => true,
        'supports' => array( 'title', 'editor','excerpt', 'comments','page-attributes'),
        'taxonomies' => array(),
        "show_in_rest" => true,
        "rest_base" => "amstelveenlijn"
    );

    register_post_type( 'amstelveenlijn', $args );
}

    add_action( 'init', 'register_amstelveenlijn' );
?>
