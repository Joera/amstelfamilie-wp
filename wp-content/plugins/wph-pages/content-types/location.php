<?php

function register_location() {

    $args = array(
        'label'  => 'Locatie',
        'labels' => array(
            'add_new_item' => 'Locatie toevoegen',
            'new_item' => 'Nieuwe locatie',
            'view_item' => 'Bekijk locatie',
            'view_items' => 'Bekijk locaties'
        ),
        'description' => '',
        'public' => true,
        'show_ui' => true,
        'has_archive' => false,
        'show_in_menu' => true,
        'exclude_from_search' => false,
        'capability_type' => 'post',
        'map_meta_cap' => true,
        'hierarchical' => false,
        'rewrite' => array( 'slug' => 'locatie', 'with_front' => true ),
        'query_var' => true,
        'supports' => array( 'title', 'editor','page-attributes'),
        "show_in_rest" => true,
        "rest_base" => "locations"
    );

    register_post_type( 'location', $args );
}

    add_action( 'init', 'register_location' );
?>
