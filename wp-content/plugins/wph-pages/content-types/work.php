<?php

function register_work() {

    $args = array(
        'label'  => 'Werkzaamheden',
        'labels' => array(
            'add_new_item' => 'Werkzaamheden toevoegen',
            'new_item' => 'Nieuw werkzaamheden',
            'view_item' => 'Bekijk werkzaamheden',
            'view_items' => 'Bekijk werkzaamheden'
        ),
        'description' => '',
        'public' => true,
        'show_ui' => true,
        'has_archive' => false,
        'show_in_menu' => true,
        'exclude_from_search' => false,
        'capability_type' => 'post',
        'map_meta_cap' => true,
        'hierarchical' => false,
        'rewrite' => array( 'slug' => 'werkzaamheden', 'with_front' => true ),
        'query_var' => true,
        'supports' => array( 'title', 'editor'), // ,'page-attributes'
        "show_in_rest" => true,
        "rest_base" => "works"
    );

    register_post_type( 'work', $args );
}

    add_action( 'init', 'register_work' );
?>
