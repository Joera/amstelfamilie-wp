<?php

function register_interview() {

    $args = array(
        'label'  => 'Interview',
        'labels' => array(
            'add_new_item' => 'Interview toevoegen',
            'new_item' => 'Nieuw interview',
            'view_item' => 'Bekijk interview',
            'view_items' => 'Bekijk interviews'
        ),
        'description' => '',
        'public' => true,
        'show_ui' => true,
        'has_archive' => false,
        'show_in_menu' => true,
        'exclude_from_search' => false,
        'capability_type' => 'post',
        'map_meta_cap' => true,
        'hierarchical' => false,
        'rewrite' => array( 'slug' => 'interview', 'with_front' => true ),
        'query_var' => true,
        'supports' => array( 'title', 'editor','excerpt', 'comments', 'author' ),
        'taxonomies' => array( 'post_tag' ),
        "show_in_rest" => true,
        "rest_base" => "interviews"
    );

    register_post_type( 'interview', $args );
}

    add_action( 'init', 'register_interview' );
?>
