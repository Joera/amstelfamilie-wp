<?php

function register_contact() {

    // register theme custom post type
    $args = array(
        'public' => true,
        'label'  => 'Contactpersoon',
        'labels' => array(
            'add_new_item' => 'Contactpersonen toevoegen',
            'new_item' => 'Nieuw contactpersoon',
            'view_item' => 'Bekijk contactpersonen',
            'view_items' => 'Bekijk contactpersonen'
        ),
        "show_in_rest" => true,
        "rest_base" => "contact",
        "has_archive" => false,
        "show_in_menu" => true,
        "menu_position" => 5,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "contact", "with_front" => false ),
        "query_var" => true,
        "supports" => array("title"),
    );
    register_post_type( 'contact', $args );
}

add_action( 'init', 'register_contact' );
?>