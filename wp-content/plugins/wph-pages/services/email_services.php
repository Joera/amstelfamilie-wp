<?php

class Email_Services {

    function setTask($mailingdb,$mailingID,$from,$email,$subject,$text,$html,$replyTo) {
        // save email tasks to database
        $cc = '';
        $status = 'unsend';
        $now = current_time('mysql');
        $sql = "INSERT INTO wp_email_tasks (mailing_id,time,_from,_to,subject,text,html,cc,reply_to,status) VALUES(?,?,?,?,?,?,?,?,?,?)";
        $stmt= $mailingdb->prepare($sql);
        $stmt->bind_param("ssssssssss",$mailingID,$now,$from,$email,$subject,$text,$html,$cc,$replyTo,$status);
        $stmt->execute();

    }

    function setJob($mailingdb,$mailingID) {

        $type = 'single';
        $postName = '';
        $status = 'unsend';

        $now = current_time( 'mysql', false);
        $scheduled = date( 'Y-m-d H:i:s', strtotime( $now ) + 10 ); // 1800

        $sql = "INSERT INTO wp_email_jobs (datetime,mailing_id,type,post_name,status) VALUES(?,?,?,?,?)";

        $stmt = $mailingdb->prepare($sql);
        $stmt->bind_param("sssss",$scheduled,$mailingID,$type,$postName,$status);
        $stmt->execute();

        return;
    }
}
