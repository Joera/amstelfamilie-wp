<?php

    $path = preg_replace('/wp-content.*$/','',__DIR__);
    require_once($path.'wp-load.php'); // load wordpress libs to enable the wordpress functions in this document

    $email_services = new Email_Services();
    $mailingdb = new mysqli('localhost','pavarotti3','N0y&r3f9', 'lijn25');
    $mailingID = uniqid().uniqid().uniqid();

    function getInvitees($mailingdb) {

            $invitees;


          //  $sql = "SELECT * FROM box WHERE NOT rsvp = 'boxonly'";
            $sql = "SELECT * FROM box WHERE voornaam = 'Wendy'";
            if ($result = $mailingdb -> query($sql)) {
                while ($row = $result -> fetch_row()) {
                $invitees[] = $row;
                }
              $result -> free_result();
            }

            return $invitees;
    }


    $invitees = getInvitees($mailingdb);

    foreach ($invitees as $invitee) {

        $subject = "Belangrijke informatiemail m.b.t. officiële online opening lijn 25";
        $from = "Amstelveenlijn<noreply@amstelveenlijn.nl>";
        $replyTo = 'amstelveenlijn.MET@amsterdam.nl';
        $email = $invitee[1];
        $text = '';
        $html = '';

        $body = new stdClass();

        $body->salutation = $invitee[2];
        $body->first_name = $invitee[3];
        $body->last_name = $invitee[4];
        $body->token = $invitee[0];

        include(PLUGIN_FOLDER . 'wph-pages/email-templates/rsvp_link_betrokkenen_zonder_antwoord.php');
        $email_services->setTask($mailingdb,$mailingID,$from,$email,$subject,$text,$html,$replyTo);
    }

    $email_services->setJob($mailingdb,$mailingID);

    wp_send_json( count($invitees), 200 );
