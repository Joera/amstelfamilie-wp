<?php

    $path = preg_replace('/wp-content.*$/','',__DIR__);
    require_once($path.'wp-load.php');

    function getUnCompletedJobs($mailingdb) {

        $table_name = "wp_email_jobs";
        $jobs = $mailingdb->query(
           "
                    SELECT *
                    FROM $table_name a
                    WHERE a.status = 'unsend'
                    AND a.datetime <= NOW()
        ");

        return $jobs;
   }

    function getUnsendTasks($mailingdb,$mailingID) {

        $status = 'unsend';
        $sql = "SELECT * FROM wp_email_tasks WHERE status = ? AND mailing_id = ? LIMIT 100";

        $stmt= $mailingdb->prepare($sql);
        $stmt->bind_param("ss",$status,$mailingID);
        $stmt->execute();

        $result = $stmt->get_result(); // get the mysqli result
        $tasks = array();
        while ($row = $result->fetch_array(MYSQLI_NUM)) {

            $task = new stdClass();

            $task->mailing_id = $row[0];
            $task->datetime = $row[1];
            $task->from = $row[2];
            $task->to = $row[3];
            $task->subject = $row[4];
            $task->text = $row[5];
            $task->html = $row[6];
            $task->cc = $row[7];
            $task->reply_to = $row[8];
            $task->status = $row[9];
            $task->mailgun_id = $row[10];

            $tasks[] = $task;
        }

        return $tasks;
    }

    function updateTask($mailingdb,$task,$mailgun_id) {

        $status = 'queued';

        $sql = "UPDATE wp_email_tasks SET status = ?, mailgun_id = ? WHERE mailing_id = ? AND _to = ? ";
        $stmt = $mailingdb->prepare($sql);
        $stmt->bind_param("ssss",$status,$mailgun_id,$task->mailing_id,$task->to);
        $stmt->execute();

        return;
    }

    function updateJob($mailingdb,$mailingID) {

        PC::debug($mailingID);

        $status = 'done';
        $sql = "UPDATE wp_email_jobs SET status = ? WHERE mailing_id = ?";
        $stmt = $mailingdb->prepare($sql);
        $stmt->bind_param("ss",$status,$mailingID);
        $stmt->execute();

        return;
    }

$mailingdb = new mysqli('localhost','pavarotti3','N0y&r3f9', 'lijn25');
$queue = array();
$openJobs = getUnCompletedJobs($mailingdb);

foreach ($openJobs as $openJob) {

    $openTasks = getUnsendTasks($mailingdb,$openJob['mailing_id']);

    if ($openTasks && count($openTasks) > 0) {
            $queue = array_merge($queue,$openTasks);
    }


}

if (count($queue) > 0) {
    $queue = array_slice($queue,0,250);
}

$count = 0;
foreach ($queue as $queueItem) {

//    PC::debug($queueItem);
    $mailgun = new Mailgun_Connector();
    $response = $mailgun->send_email($queueItem->from, $queueItem->to, $queueItem->subject, $queueItem->text, $queueItem->html, $queueItem->cc, $queueItem->reply_to);

    if($response) {
        updateTask($mailingdb, $queueItem, json_decode($response,false)->id);
        $count++;
    }

}

foreach ($openJobs as $openJob) {
    $openTasks = getUnsendTasks($mailingdb,$openJob['mailing_id']);
    if(!$openTasks || count($openTasks) < 1) {
       updateJob($mailingdb,$openJob['mailing_id']);
    }
}

wp_send_json( $count, 200 );
