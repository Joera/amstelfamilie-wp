<?php 

    add_action('init','convert_interview');

    function convert_interview(){

        global $wpdb;

        $args = array(
            'post_type' => 'interview',
            'posts_per_page' => -1,
        );

        $posts = get_posts($args);

        foreach ( $posts as $post ) {

            $title = get_the_title( $post->ID );
            $quote = get_field('citaat',$post->ID);
            $newTitle = $title . ': "' . $quote . '"';

            $where = array( 'ID' => $post->ID );
            $wpdb->update( $wpdb->posts, array( 'post_title' => $newTitle, 'post_type' => 'post' ), $where );
            wp_set_object_terms( $post->ID, array('interview'), 'category', true );

            update_field('render-environments', ['amstelveenlijn'], $post->ID);
        }
    }


