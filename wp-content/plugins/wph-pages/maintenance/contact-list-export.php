<?php
require '/var/www/vhosts/wijnemenjemee.nl/cms.amsteltram.nl/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


add_action( 'manage_posts_extra_tablenav', 'display_export_button', 20, 1 );

function display_export_button( $which )
{
    global $typenow;

    if ( 'contact' === $typenow && 'top' === $which ) { ?>

        <div class="alignleft actions"><?php
	submit_button('Export', 'primary', 'export-xls', false, array(
	        'style' => 'margin: 1px 8px 0 0;',
            'onclick' => 'window.open("https://cms.amsteltram.nl/wp-json/avl/v1/export_contacts")'
    ));
	?></div>

        <?php
    }
}


add_action('rest_api_init', function() {
	register_rest_route( 'avl/v1', '/export_contacts', array(
		'methods' => array('POST', 'GET'),
		'callback' => function() {

		    $spreadsheet = new Spreadsheet();
			$sheet = $spreadsheet->getActiveSheet();

			global $languageChoices;
			global $ageChoices;

		    // What data do we want?
            $columns = array(
                    array(
                        'order' => 1,
                        'column' => 'A',
                        'key' => 'contact_email',
                        'label' => 'Template',
                        'is_empty' => true,
                        'has_choices' => false,
                        'is_child' => false
                    ),
                    array(
                        'order' => 2,
                        'column' => 'B',
                        'key' => 'contact_working_language',
                        'label' => 'Taal',
                        'is_child' => false,
                        'has_choices' => true,
                        'choices' => $languageChoices
                    ),
                    array(
                        'order' => 3,
                        'column' => 'C',
                        'key' => 'contact_email',
                        'label' => 'E-mail',
                        'is_child' => false,
                        'has_choices' => false,
                        'is_empty' => false
                    ),
                    array(
                        'order' => 4,
                        'column' => 'D',
                        'key' => 'contact_salutation',
                        'label' => 'Aanhef',
                        'is_empty' => false,
                        'has_choices' => false,
                        'is_child' => false
                    ),
                    array(
                        'order' => 5,
                        'column' => 'E',
                        'key' => 'contact_first_name',
                        'label' => 'Voornaam',
                        'is_empty' => true,
                        'has_choices' => false,
                        'is_child' => false
                    ),
                    array(
                        'order' => 5,
                        'column' => 'F',
                        'key' => 'contact_last_name',
                        'label' => 'Achternaam',
                        'is_empty' => false,
                        'has_choices' => false,
                        'is_child' => false
                    ),
                    array(
                        'order' => 6,
                        'column' => 'G',
                        'key' => 'contact_postal_code',
                        'label' => 'Postcode',
                        'is_empty' => false,
                        'has_choices' => false,
                        'is_child' => false
                    ),
                    array(
                        'order' => 6,
                        'column' => 'H',
                        'key' => 'contact_age_group',
                        'label' => 'Leeftijdsgroep',
                        'is_empty' => false,
                        'is_child' => false,
                        'has_choices' => true,
                        'choices' => $ageChoices
                    ),
                    array(
                        'order' => 7,
                        'column' => 'I',
                        'key' => 'commuter',
                        'label' => 'Forens',
                        'is_empty' => false,
                        'has_choices' => false,
                        'is_child' => true
                    ),
                    array(
                        'order' => 8,
                        'column' => 'J',
                        'key' => 'public_transport_user',
                        'label' => 'Openbaar vervoer reiziger',
                        'is_empty' => false,
                        'has_choices' => false,
                        'is_child' => true
                    ),
                    array(
                        'order' => 9,
                        'column' => 'K',
                        'key' => 'car_owner',
                        'label' => 'Automobilist',
                        'is_empty' => false,
                        'has_choices' => false,
                        'is_child' => true
                    ),
                    array(
                        'order' => 10,
                        'column' => 'L',
                        'key' => 'cyclist',
                        'label' => 'Fietser',
                        'is_empty' => false,
                        'has_choices' => false,
                        'is_child' => true
                    ),
                    array(
                        'order' => 11,
                        'column' => 'M',
                        'key' => 'pedestrian',
                        'label' => 'Voetganger',
                        'is_empty' => false,
                        'has_choices' => false,
                        'is_child' => true
                    ),
                    array(
                        'order' => 12,
                        'column' => 'N',
                        'key' => 'amstelveen_resident',
                        'label' => 'Omwonende',
                        'is_empty' => false,
                        'has_choices' => false,
                        'is_child' => true
                    ),
                    array(
                        'order' => 13,
                        'column' => 'O',
                        'key' => 'area_resident',
                        'label' => 'Inwoner omliggende gemeente',
                        'is_empty' => false,
                        'has_choices' => false,
                        'is_child' => true
                    ),
                    array(
                        'order' => 14,
                        'column' => 'P',
                        'key' => 'company',
                        'label' => 'Bedrijf',
                        'is_empty' => false,
                        'has_choices' => false,
                        'is_child' => true
                    ),
                    array(
                        'order' => 15,
                        'column' => 'Q',
                        'key' => 'meetmoment',
                        'label' => 'Type meetmoment',
                        'is_empty' => true,
                        'has_choices' => false,
                        'is_child' => false
                    ),
                    array(
                        'order' => 16,
                        'column' => 'R',
                        'key' => 'date',
                        'label' => 'Datum',
                        'is_empty' => false,
                        'has_choices' => false,
                        'is_child' => false
                    ),
                    array(
                        'order' => 17,
                        'column' => 'S',
                        'key' => 'contact_provenance',
                        'label' => 'Herkomst',
                        'is_empty' => false,
                        'has_choices' => false,
                        'is_child' => false
                    )

            );

			$columnCharacter = 'a';

            foreach ($columns as $column) {
                $sheet->setCellValue(strtoupper($columnCharacter).'1', $column['label']);
				$columnCharacter = ++$columnCharacter;
            }

            // Loop posts
            $posts = get_posts( array(
                'post_type'   => 'contact',
                'numberposts' => -1,
            ) );

			foreach ( $posts as $key => $post ) {
			    $count = $key+2;
                foreach ($columns as $column) {
                    if ($column['is_child'] === true) {
	                    $value = get_post_meta($post->ID, 'contact_interests', true);
	                    if (in_array($column['key'], $value)) {
                            $sheet->setCellValue($column['column'] . $count, 'x');
                        }
                    } else if ($column['has_choices'] === true) {
	                    $value = get_post_meta($post->ID, $column['key'], true);
	                    if($value === '') {
                            $sheet->setCellValue($column['column'].$count,'');
                        } else {
                            $sheet->setCellValue($column['column'] . $count, $column['choices'][$value]);
                        }
                    } else if ($column['is_empty'] === true) {
                        $sheet->setCellValue($column['column'] . $count, '');
                    } else if ($column['key'] == 'date' ){
                        $sheet->setCellValue($column['column'] . $count, get_the_date('d/m/Y',$post->ID));
                    } else {
	                    $value = get_post_meta($post->ID, $column['key'], true);
	                    $language = get_post_meta($post->ID, 'contact_working_language', true);

	                    if($language === 'english' && $value === 'heer') {
	                        $value = 'Mr.';
                        } else if($language === 'english' && $value === 'mevrouw') {
                            $value = 'Mrs.';
                        }

                        $sheet->setCellValue($column['column'].$count, $value);

                    }
                }
			}

			$spreadsheet->getActiveSheet()->setAutoFilter('A1:E'.$count);
			$filename = 'Contacten-'.current_time('Y-m-d-H-i-s').'.xlsx';

            $writer = new Xlsx($spreadsheet);
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment; filename="'.$filename.'"');
			$writer->save("php://output");

		},
	) );
});