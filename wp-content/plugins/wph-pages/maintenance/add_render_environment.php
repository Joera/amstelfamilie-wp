<?php 

    add_action('init','update_render_envs');

    function update_render_envs(){


        $args = array(
            'post_type' => 'post',
            'posts_per_page' => -1,
        ); 

        $posts = get_posts($args); 

        foreach ( $posts as $post ) {

            $envs = get_field('render-environments',$post->ID);

//            if(!is_array($envs)) {
//                $envs = [];
//            }
//
//            if(!in_array('amstelveenlijn',$envs)) {
//                $envs[] = 'amstelveenlijn';
//            }

            if($envs == null) {

                $envs = ['amstelveenlijn'];
            }

//            if(!in_array('Array',$envs)) {
//                $envs[] = 'amstelveenlijn';
//            }

            update_field('render-environments', $envs, $post->ID);
        }
    }
