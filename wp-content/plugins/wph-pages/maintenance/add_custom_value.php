<?php 

    add_action('init','update_my_metadata');
    function update_my_metadata(){

        $args = array(
            'date_query' => array(
                'after' => date('Y-m-d', strtotime('-100 days')) 
            )
        ); 

        $posts = get_posts($args); 

        foreach ( $posts as $post ) {

            $comments = get_comments($post->ID);

            foreach ( $comments as $comment ) {

                $authorId = $post->post_author;
                $author = get_the_author_meta('user_nicename',$authorId);

                if (get_comment_meta( $comment->comment_ID, 'assignedto', true )) {
                    
                    update_comment_meta( $comment->comment_ID,'assignedto', $author);

                } else { 

                    add_comment_meta( $comment->comment_ID,'assignedto', $author);
                }

                if (get_comment_meta( $comment->comment_ID, 'answered', true )) {
                    
                    update_comment_meta( $comment->comment_ID,'answered',1);

                } else { 

                    add_comment_meta( $comment->comment_ID,'answered',1);
                }
            }
        }
    }
