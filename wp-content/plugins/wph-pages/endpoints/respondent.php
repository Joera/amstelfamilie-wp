<?php
defined( 'ABSPATH' ) or die( 'You can\'t access this file directly!');

define('PLUGIN_ROOT_PATH', __DIR__.'/../');

class WPH_RespondentForm {
    public function init() {
        $this->hooks();
    }

    public function hooks() {
        add_action('rest_api_init', array($this, 'register_endpoint_add_respondent'));
        add_action('rest_api_init', array($this, 'register_endpoint_get_respondent'));
        add_action('rest_api_init', array($this, 'register_endpoint_remove_respondent'));
    }

    public function register_endpoint_get_respondent() {
        register_rest_route( 'wp/v2', '/respondent', array(
            'methods' => WP_REST_Server::READABLE,
            'callback' => array($this, 'getRespondent')
        ) );
    }

    public function register_endpoint_add_respondent() {
        register_rest_route( 'wp/v2', '/respondent', array(
            'methods' => 'POST',
            'callback' => array($this, 'addRespondent')
        ) );
    }

    public function register_endpoint_remove_respondent() {
        register_rest_route( 'wp/v2', '/respondent', array(
            'methods' => 'DELETE',
            'callback' => array($this, 'removeRespondent')
        ) );
    }

    public function removeRespondent($request) {

        $token = json_decode($request->get_body())->token;

        $args = array(
            'post_type' => 'contact',
            'post_status' => 'publish',
            'meta_query' => array(
                array(
                    'key' => 'contact_token',
                    'value' => $token,
                    'compare' => 'LIKE'
                )
            )
        );

        $query = new WP_Query($args);
        wp_delete_post($query->posts[0]->ID);

        return new WP_REST_Response(json_encode('Uw gegevens zijn verwijderd'),200);
    }

    // get details of a single subscriber
    public function getRespondent($request) {

        $params = $request->get_params();

        if ($params['token'] && $params['token'] != null) {

            $args = array(
                'post_type' => 'contact',
                'post_status' => 'publish',
                'meta_query' => array(
                    array(
                        'key' => 'contact_token',
                        'value' => $params['token'],
                        'compare' => 'LIKE'
                    )
                )
            );

        } else {
            return array('status' => 200, 'response' => 'please supply email or identifier');
        }


        $query = new WP_Query($args);
        $contact = $query->post; // get post data of subscriber

        if ($contact) {
            $controller = new WP_REST_Posts_Controller('contact');
            $data = $controller->prepare_item_for_response($contact, $request);
            return new WP_REST_Response($data, 200);
        } else {
            return array('status' => 200, 'response' => 'no such token');
        }
    }

    // add new subscriber
    public function addRespondent($request) {

       // $mail = new Mail_Sender();

        $body = json_decode($request->get_body());
        // onderstaande aanpassen met al dan niet hidden field
        $body->panel = true;

            $args = array(
                'post_type'     => 'contact',
                'post_status'   => 'publish',
                'meta_query' => array(
                    array(
                        'key' => 'contact_email',
                        'value' => $body->email,
                        'compare' => '='
                    )
                )
            );
            $query = new WP_Query($args);

            // check if email address is already subscriber
            if($query->post_count > 0 && $body->token == "") {
                $post_id = $query->posts[0]->ID;
                $token = get_post_meta($post_id, 'contact_token')[0];

                $emailController = new Email_Controller();
                $emailController->subscription_update($body,$token);

                return new WP_REST_Response('Dit e-mail adres is al ingeschreven. U ontvangt een email waarmee u uw aanmelding kunt wijzigen.', 200);

            } else if ($query->post_count > 0 && $body->token != "") {

                $post_id = $query->posts[0]->ID;
                update_post_meta( $post_id, 'contact_email', $body->email );
                update_post_meta( $post_id, 'contact_panel', $body->panel );
                update_post_meta( $post_id, 'contact_salutation', $body->salutation );
                update_post_meta( $post_id, 'contact_last_name', $body->last_name );
                update_post_meta( $post_id, 'contact_postal_code', $body->postal_code );
                update_post_meta( $post_id, 'contact_working_language', $body->working_language );
                update_post_meta( $post_id, 'contact_age_group', $body->age_group );
                update_post_meta( $post_id, 'contact_interests', $body->interests  );

                update_post_meta( $post_id, 'contact_provenance', 'website');

                update_post_meta( $post_id, 'contact_subscription', $body->subscription  );
                update_post_meta( $post_id, 'contact_gvb', $body->gvb  );

                // update subscriber

                return new WP_REST_Response(json_encode('Uw gegevens zijn gewijzigd'), 200);



            } else {
                // Add new subscriber
                // new subscriber data
                $new_subscriber = array(
                    'post_title' => $body->email,
                    'post_content' => '',
                    'post_status' => 'publish',
                    'post_type' => 'contact',
                );

                // generate token
                $token = uniqid().uniqid().uniqid();
                $post_id = wp_insert_post( $new_subscriber );

                add_post_meta( $post_id, 'contact_email', $body->email );
                add_post_meta( $post_id, 'contact_token', $token );
                add_post_meta( $post_id, 'contact_panel', $body->panel );
                add_post_meta( $post_id, 'contact_salutation', $body->salutation );
                add_post_meta( $post_id, 'contact_last_name', $body->last_name );
                add_post_meta( $post_id, 'contact_postal_code', $body->postal_code );
                add_post_meta( $post_id, 'contact_working_language', $body->working_language );
                add_post_meta( $post_id, 'contact_age_group', $body->age_group );
                add_post_meta( $post_id, 'contact_interests', $body->interests  );

                add_post_meta( $post_id, 'contact_provenance', 'website'  );
                add_post_meta( $post_id, 'contact_subscription', $body->subscription  );
                add_post_meta( $post_id, 'contact_gvb', $body->gvb  );

//                $emailController = new Email_Controller();
//                $emailController->subscription_confirmation($body,$token);

                return new WP_REST_Response(json_encode('Uw aanmelding is succesvol verzonden'),200);
            }

    }
}

$wph_respondentform = new WPH_RespondentForm();
$wph_respondentform->init();