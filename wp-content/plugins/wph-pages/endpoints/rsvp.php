<?php
defined( 'ABSPATH' ) or die( 'You can\'t access this file directly!');

// define('PLUGIN_ROOT_PATH', __DIR__.'/../');

class WPH_RSVPForm {
    public function init() {
           $this->hooks();
    }

    public function hooks() {
        add_action('rest_api_init', array($this, 'register_endpoint_get_rsvp'));
        add_action('rest_api_init', array($this, 'register_endpoint_change_rsvp'));
       // add_action('rest_api_init', array($this, 'register_endpoint_remove_rsvp'));
    }

    public function register_endpoint_get_rsvp() {
        register_rest_route( 'wp/v2', '/rsvp', array(
            'methods' => WP_REST_Server::READABLE,
            'callback' => array($this, 'getRSVP')  // getRSVP
        ) );
    }

    public function register_endpoint_change_rsvp() {
        register_rest_route( 'wp/v2', '/rsvp', array(
            'methods' => 'POST',
            'callback' => array($this, 'changeRSVP')
        ) );
    }

//     public function register_endpoint_remove_respondent() {
//         register_rest_route( 'wp/v2', '/respondent', array(
//             'methods' => 'DELETE',
//             'callback' => array($this, 'removeRespondent')
//         ) );
//     }



    // get details of a single subscriber
    public function getRSVP($request) {
        $mailingdb = new mysqli('localhost','pavarotti3','N0y&r3f9', 'lijn25');
        $params = $request->get_params();

        if ($params['token'] && $params['token'] != null) {

                $sql = "SELECT * FROM box WHERE token=?";
                $stmt = $mailingdb->prepare($sql);
                $stmt->bind_param("s", $params['token']);
                $stmt->execute();
                $result = $stmt->get_result(); // get the mysqli result
                $user = $result->fetch_assoc(); // fetch data

                if ($user) {
                      return array('status' => 200, 'response' => $user);
                } else {
                    return array('status' => 200, 'response' => 'token not recognized');
                }
        } else {
            return array('status' => 200, 'response' => 'please supply a token');
        }


    }

    // add new subscriber
    public function changeRSVP($request) {

      //  return new WP_REST_Response('Uw gegevens zijn gewijzigd', 200);

         $body = json_decode($request->get_body());
//          $body->rsvp = 'nobox';
         $mailingdb = new mysqli('localhost','pavarotti3','N0y&r3f9', 'lijn25');
         $sql = "UPDATE box SET aanhef=?,email=?,rsvp=?,straatnaam=?,huisnummer=?,postcode=?,woonplaats=?,privacy=? WHERE token=?"; //
         $stmt = $mailingdb->prepare($sql);
         $stmt->bind_param("sssssssis",$body->aanhef,$body->email,$body->rsvp,$body->straatnaam,$body->huisnummer,$body->postcode,$body->woonplaats,intval($body->privacy),$body->token);
         $stmt->execute();

         $noRows = $stmt->affected_rows;

         if ($stmt->affected_rows == 1) {
                return new WP_REST_Response('Uw gegevens zijn gewijzigd', 200);
         } else {
                return new WP_REST_Response('Er is iets mis gegaan', 304);
         }
    }

    public function addTokens() {

            function uniqidReal($lenght = 13) {
                // uniqid gives 13 chars, but you could adjust it to your needs.
                if (function_exists("random_bytes")) {
                    $bytes = random_bytes(ceil($lenght / 2));
                } elseif (function_exists("openssl_random_pseudo_bytes")) {
                    $bytes = openssl_random_pseudo_bytes(ceil($lenght / 2));
                } else {
                    throw new Exception("no cryptographically secure random function available");
                }
                return substr(bin2hex($bytes), 0, $lenght);
            }

            $mailingdb = new mysqli('localhost','pavarotti3','N0y&r3f9', 'lijn25');

            $sql = "SELECT * FROM box";

            if ($result = $mailingdb -> query($sql)) {
                while ($row = $result -> fetch_row()) {
                    $invitees[] = $row;
                }
                $result -> free_result();
            }

            foreach ($invitees as &$invitee) {

                 $token = uniqidReal().uniqidReal();
                 $sql = "UPDATE box SET token=? WHERE email=?";
                 $stmt = $mailingdb->prepare($sql);
                 $stmt->bind_param("ss", $token, $invitee[1]);
                 $stmt->execute();
            }

            return array('status' => 200, 'response' => $invitees);

        }
}

$wph_rsvpform = new WPH_RSVPForm();
$wph_rsvpform->init();

