<?php

add_filter('manage_edit-post_columns', 'custom_posts_table_head');

function custom_posts_table_head( $columns ) {

    $columns['omgevingen']  = 'omgevingen';
    $columns['documents']  = 'documenten';
    return $columns;
}

add_action( 'manage_post_posts_custom_column', 'documents_table_content', 10, 2);

function documents_table_content( $column_name, $post_id ) {

    if ($column_name == 'documents') {

        if (have_rows('section', $post_id)):
            while (have_rows('section', $post_id)) : the_row();
                if (get_row_layout() == 'documents'):
                    $documents = [];
                    if (have_rows('files')) :
                        while (have_rows('files')) : the_row();

                            $fileObject = get_sub_field('document_clone');
                            echo "<a target='_blank' href='http://cms.publikaan.nl/wp-admin/post.php?post=" . $fileObject["file"]["ID"] . "&action=edit'>" . $fileObject["file-name"] . "</a>";
                            echo '<br/>';

                        endwhile;
                    endif;
                endif;
            endwhile;
        endif;

    } elseif ($column_name == 'omgevingen') {

        $envs = get_field('render-environments',$post_id);

        if(is_array($envs)) {
            foreach ($envs as &$env) {

                echo $env;
                echo '<br/>';
            }
        }
    }

    // var_dump($documents);
}

