<?php

add_filter('manage_edit-work_columns', 'works_table_head');
add_action( 'manage_work_posts_custom_column', 'works_table_content', 10, 2);

function works_table_head( $columns ) {


    $columns['start']  = 'Start';
    $columns['einde']  = 'Einde';
    $columns['environments'] = "Omgevingen";
    $columns['locations']  = 'Locaties';
    return $columns;
}

function works_table_content( $column_name, $post_id ) {

 if($column_name === 'environments') {

        $envs = get_field('render-environments', $post_id  );

         if(is_array($envs)) {
            foreach ($envs as &$env) {
                echo $env;
                echo '<br/>';
            }
        }
    }

    if($column_name === 'locations') {

        if ($wp_terms = wp_get_post_terms($post_id, 'locations')) {
            foreach ($wp_terms as $wp_term) {

                $args = array(
                    'name' => $wp_term->slug,
                    'post_type' => 'location',
                    'post_status' => 'publish',
                    'posts_per_page' => 1
                );

                $location = get_posts($args);
                if($location && $location[0]) {
                    echo $location[0]->post_title;
                    echo ', ';
                }
            }
        } else {
            echo 'geen locaties';
        }
    }

    if ($column_name == 'start') {
        echo get_field('start-date',$post_id); // get all the rows
    }

    if ($column_name == 'einde') {
        echo get_field('end-date',$post_id); // get all the rows
    }

}


//function order_states_for_works( $query ){
//    if( !is_admin() )
//        return;
//
//    $screen = get_current_screen();
//    if( is_object($screen->base)
//        && 'edit' == $screen->base
//        && 'work' == $screen->post_type
//        && !isset( $_GET['orderby'] ) ){
//        $query->set( 'orderby', 'date' );
//        $query->set( 'order', 'DESC' );
//    }
//}
//add_action( 'pre_get_posts', 'order_states_for_works' );

//add_action( 'manage_work_posts_custom_column', 'locations_table_content', 10, 2);

//function locations_table_content( $column_name, $post_id ) {
//
//    if($column_name === 'locations') {
//
//        if ($wp_terms = wp_get_post_terms($post_id, 'locations')) {
//            foreach ($wp_terms as $wp_term) {
//
//                $args = array(
//                    'name' => $wp_term->slug,
//                    'post_type' => 'location',
//                    'post_status' => 'publish',
//                    'posts_per_page' => 1
//                );
//
//                $location = get_posts($args);
//                echo $location[0]->post_title;
//                echo '<br/>';
//            }
//        } else {
//            echo 'geen locaties';
//        }
//    } else if($column_name === 'start') {
//
//        $start = DateTime::createFromFormat('Y-m-d', get_field('start-date', $post_id));
//        echo $start->format('d-m-Y');
//
//    } else if($column_name === 'einde') {
//
//        $end = DateTime::createFromFormat('Y-m-d', get_field('end-date', $post_id));
//        echo $end->format('d-m-Y');
//    }
//}
