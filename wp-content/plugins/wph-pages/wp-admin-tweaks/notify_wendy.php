<?php

    function filter_comment_notification_headers( $message_headers )
    {
    $notify_other_people = array(
    '"Wendy van der Meulen" <w.van.der.meulen@amsterdam.nl>'
    );

    $message_headers .= "\n" .
    'CC: ' . implode(', ', $notify_other_people );

    return $message_headers;
}


