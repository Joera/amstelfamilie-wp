<?php


add_action( 'restrict_manage_posts', 'add_environment_filter_dropdown' );


function add_environment_filter_dropdown(){
    $type = 'post';
    if (isset($_GET['post_type'])) {
        $type = $_GET['post_type'];
    }

    if ('post' == $type || 'work' == $type){
        $values = array(
            'Alles' => 'alles',
            'Amstelveenlijn' => 'amstelveenlijn',
            'Uithoornlijn' => 'uithoornlijn',
            'Amsteltram' => 'amsteltram',
        );
        ?>
        <select name="env">
            <option value=""><?php _e('Filter op ', 'wose45436'); ?></option>
            <?php
            $current_v = isset($_GET['env'])? $_GET['env']:'';
            foreach ($values as $label => $value) {
                printf
                (
                    '<option value="%s"%s>%s</option>',
                    $value,
                    $value == $current_v? ' selected="selected"':'',
                    $label
                );
            }
            ?>
        </select>
        <?php
    }
}


add_filter( 'parse_query', 'environment_filter' );


function environment_filter( $query ){
    global $pagenow;


    $type = 'post';
    if (isset($_GET['post_type'])) {
        $type = $_GET['post_type'];
    }
    if ( ('post' == $type || 'work' == $type) && is_admin() && $pagenow=='edit.php' && isset($_GET['env']) && $_GET['env'] != '') {

        if ($_GET['env'] != 'alles') {

            $meta_query = (array)$query->get('meta_query');

            $meta_query[] = array(
                'key'     => 'render-environments',
                'value'   => $_GET['env'],
                'compare' => 'LIKE',
            );

            $query->set('meta_query',$meta_query);
        }
    }
}
