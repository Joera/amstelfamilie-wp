<?php
/* 
* Can I please click those links?
*
*/
add_filter('post_link', 'wph_replace_permalink', 10, 3 );
add_filter('page_link', 'wph_replace_permalink', 10, 3 );
add_filter('post_type_link', 'wph_replace_permalink', 10, 3 );

function wph_replace_permalink($url, $post, $leavename = false) {
    $parsed = parse_url(get_site_url());
    $urlbase = '';

    if (is_int($post)) {
        $post = get_post($post);
    }

    $envs = get_field('render-environments',$post->ID );

    if ($envs && $envs[0]) {
        // $urlbase = $parsed['scheme'].'://'.$parsed['host'].'/';
        $urlbase = $parsed['scheme'] . '://' . $envs[0] . '.nl/';
    }

    $languageObject = apply_filters( 'wpml_post_language_details', NULL, $post->ID ) ;

    if($languageObject['language_code'] == 'en') {
        $urlbase = $urlbase . 'en/';
    }

    if ( $post->post_type == 'post' ) {
        $year = date("Y", strtotime($post->post_date));
        $url = $urlbase.'nieuws/'.$year.'/'.$post->post_name.'/';
    }

    else if ($post->post_type == 'page') {
        $url = $urlbase.$post->post_name.'/';
    }

    else if ($post->post_type == 'amstelveenlijn') {
        $url = $parsed['scheme'] . '://amstelveenlijn.nl/' .$post->post_name.'/';
    }

    else if ($post->post_type == 'uithoornlijn') {
        $url = $parsed['scheme'] . '://uithoornlijn.nl/' .$post->post_name.'/';
    }

    else if ($post->post_type == 'amsteltram') {
        $url = $parsed['scheme'] . '://amsteltram.publikaan.nl/' .$post->post_name.'/';
    }




	return $url;
};