<?php

    add_action('load-edit-comments.php', 'extra_comment_columns_load');

    function extra_comment_columns_load() {

        $screen = get_current_screen();
        add_filter("manage_{$screen->id}_columns", 'altered_comment_column_headers');
        add_action("manage_comments_custom_column", 'altered_comment_columns',10,2);
    }

    function altered_comment_column_headers( $columns) {

//        $columns = array(
//            'cb' => '<input type="checkbox" />',
//            'response' => __( 'In reactie op'),
//            'author' => __( 'Auteur' ),
//            'comment' => __( 'Comment' ),
//            'assignedto' => __( 'voor' ),
//            'answered' => __( 'vink' ),
//
//            // 'rating' => __( 'Waardering' )
//        );

        $new = 'Bericht';


        array_splice( $columns, 2, 0, $new );
        array_splice($columns, 4, 1);

        return $columns;
    }

function capitalize($item){
    if($item)  { return ucfirst($item); } else { return 'Amstelveenlijn';}
}

function altered_comment_columns($column,$commentID) {
//    global $post;


    switch( $column ) {

        case 'Bericht' :

            $comment = get_comment($commentID);
            $post_id = $comment->comment_post_ID;
            $post_link = '';
            $envs = get_field('render-environments',$post_id );



            if ($envs) {
                $envs = array_map("capitalize",$envs);
                $post_link .= "<b>" . implode(",", $envs) . ":</b>";
            }

            if(get_post_type($post_id) == 'amstelveenlijn') {
                $post_link .= "<b>Amstelveenlijn:</b>";
            }

            if(get_post_type($post_id) == 'uithoornlijn') {
                $post_link .= "<b>Uithoornlijn:</b>";
            }

            if(get_post_type($post_id) == 'amsteltram') {
                $post_link .= "<b>Amsteltram:</b>";
            }

            $post_link .= "<a href='" . esc_url( get_edit_post_link( $post_id ) ) . "'>";

            $post_link .= " " . esc_html( get_the_title( $post_id ) ) . '</a>';

            echo $post_link;

            echo '<br/><a class="online" href="' . get_the_permalink($post_id) . '">bekijk op website</a>';

//            if ( $comment->comment_parent ) {
//                $parent      = get_comment( $comment->comment_parent );
//                $parent_link = esc_url( get_comment_link( $parent ) );
//                $name        = get_comment_author( $parent );
//                printf(
//                /* translators: %s: comment link */
//                    '<br/>' . __( 'In reply to %s.' ),
//                    '<a href="' . $parent_link . '">' . $name . '</a>'
//                );
//            }

            break;

        /* Just break out of the switch statement for everything else. */
        default :
            break;
    }
}