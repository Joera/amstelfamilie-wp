<?php

    function add_acf_layouts($layouts) {

        foreach(glob(ABSPATH . 'wp-content/plugins/wph-pages/field-groups/sections/*/acf.php') as $file){
            require $file;
        }
        return $layouts;
    }

    function add_sections_to_api($sections,$name,$sectionOrder) {

        foreach(glob(ABSPATH . 'wp-content/plugins/wph-pages/field-groups/sections/*/api.php') as $file){
            require $file;
        }

        return $sections;
    }
?>