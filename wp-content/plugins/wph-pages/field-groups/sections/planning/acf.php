<?php

$layouts[] = array (
    'key' => '5a2eecc844441c6',
    'label' => 'Planning',
    'name' => 'datavisplanning',
    'display' => 'row',
    'min' => '',
    'max' => '',
    'sub_fields' => array (
        array (
            'key' => 'field_5953315c555555ttce',
            'label' => 'Optionele titel',
            'name' => 'title',
            'type' => 'text'
        ),
        array (
            'key' => 'datavis-planning',
            'label' => 'Planning data',
            'name' => 'planning',
            'type' => 'planning',
            'initial_value' => '[]',
            'column_width' => '',
            'default_value' => '[]',
            'toolbar' => 'full',
            'media_upload' => 'no',
        ),
        array (
            'key' => 'datavis-instance',
            'label' => 'Kies bestaande planning',
            'name' => 'instance',
            'type' => 'select',
            'default_value' => 'none',
            'choices' => array(
                'none'	=> 'Geen',
                'amsteltram'	=> 'Amsteltram',
                'amstelveenlijn'	=> 'Amstelveenlijn',
                'uithoornlijn'	=> 'Uithoornlijn'
            )
        )
    ),
);


?>