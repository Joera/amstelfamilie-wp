<?php

    if ($name == 'datavisplanning') {

        $sections->$sectionOrder->optional_title =  get_sub_field('title');
        $sections->$sectionOrder->dataset = json_decode(urldecode(get_sub_field('planning')));
        $sections->$sectionOrder->instance = get_sub_field('instance');

    }
