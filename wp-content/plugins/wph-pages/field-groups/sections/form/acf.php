<?php

$layouts[] = array (
    'key' => 'acf_sections_form',
    'label' => 'Form',
    'name' => 'sections_form',
    'display' => 'row',
    'min' => '',
    'max' => '',
    'sub_fields' => array (
        array (
            'key' => 'acf_sections_form_form',
            'label' => 'Formulier',
            'name' => 'sections_form_form',
            'type' => 'select',
            'choices' => array(
                'contact'	=> 'Contactformulier',
                'feedback' => 'Tevredenheidmonitor Amstelveenlijn',
                'newsletter_uithoornlijn' => 'Nieuwsbrief Uithoornlijn'
            ),
        ),
    ),
);


?>