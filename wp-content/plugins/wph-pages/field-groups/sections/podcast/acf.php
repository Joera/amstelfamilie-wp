<?php

$layouts[] = array (
    'key' => 'acf_sections_podcast',
    'label' => 'Podcast',
    'name' => 'sections_podcast',
    'display' => 'row',
    'min' => '',
    'max' => '',
    'sub_fields' => array (
        array (
            'key' => 'acf_sections_podcast_audiofile',
            'label' => 'Audio-bestand',
            'name' => 'sections_podcast_audiofile',
            'type' => 'text'
        ),
    ),
);


?>
