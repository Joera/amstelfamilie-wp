<?php

$layouts[] = array (
    'key' => 'acf_work_card',
    'label' => 'Werkzaamheden kaartje',
    'name' => 'work_card',
    'display' => 'row',
    'min' => '',
    'max' => '',
    'sub_fields' => array (
        array (
            'key' => 'acf_work_card',
            'label' => 'Werkzaamheden',
            'name' => 'acf_work_card',
            'type' => 'post_object',
            'post_type' => array (
                0 => 'work',
            ),
            'taxonomy' => array (
                0 => 'all',
            ),
            'allow_null' => 0,
            'multiple' => 0,
        ),
    ),
);


?>