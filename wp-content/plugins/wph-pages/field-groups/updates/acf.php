<?php

if(function_exists("register_field_group"))
{
    register_field_group(array (
        'id' => 'acf_updates',
        'title' => 'Updates',
        'fields' => array (
            array (
            'key' => 'acf_updates_repeater',
            'label' => '',
            'name' => 'updates_repeater',
            'type' => 'repeater',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'collapsed' => '',
            'min' => 0,
            'max' => 10,
            'layout' => 'row',
            'button_label' => 'Update toevoegen',
            'sub_fields' => array(
                array(
                    'key' => 'acf_update_date',
                    'label' => 'Datum',
                    'name' => 'update_date',
                    'type' => 'date_picker',
                    'first_day' => 1,
                    'return_format' => 'd/m/Y',
                    'display_format' => 'd/m/Y',
                    'save_format' => 'yy-mm-dd',
                ),
                array(
                    'key' => 'acf_update_title',
                    'label' => 'Titel',
                    'name' => 'update_title',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'acf_update_content',
                    'label' => 'Tekst',
                    'name' => 'update_content',
                    'type' => 'textarea',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
            ),
            ),
        ),
        'location' => configToLocations('updates'),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
}

?>