<?php

add_action( 'init', 'register_updates' );

function register_updates() {
    register_rest_field( json_decode(WPH_CONFIG)->field_groups->updates,
        'updates',
        array(
            'get_callback'    => 'get_updates',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_updates( $object, $field_name, $request ) {

    $updates = array();

    if (have_rows('updates_repeater',$object['id'])): // check if the flexible content field has rows of data
        while ( have_rows('updates_repeater', $object['id']) ) : the_row();

            $update = new stdClass();
            $update->date = get_sub_field('update_date');
            $update->title = get_sub_field('update_title');
            $update->content = get_sub_field('update_content');

            array_push($updates,$update);

        endwhile;
    endif;

    return array_reverse($updates);
}