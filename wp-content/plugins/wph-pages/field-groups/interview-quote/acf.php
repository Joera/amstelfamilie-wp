<?php

if(function_exists("register_field_group"))
{
    register_field_group(array (
        'id' => 'acf_banner',
        'title' => 'Banner',
        'fields' => array (
            array (
                'key' => 'field_54de01507cdc3',
                'label' => 'Citaat',
                'name' => 'citaat',
                'type' => 'wysiwyg',
                'default_value' => '',
                'toolbar' => 'full',
                'media_upload' => 'yes',
            ),
        ),
        'location' => configToLocations('interview_quote'),
        'options' => array (
            'position' => 'normal',
            'layout' => 'no_box',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
}

?>