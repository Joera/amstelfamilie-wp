<?php

add_action( 'init', 'register_interview_quote' );

function register_interview_quote() {
    register_rest_field( json_decode(WPH_CONFIG)->field_groups->interview_quote,
        'quote',
        array(
            'get_callback'    => 'get_interview_quote',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_interview_quote( $object, $field_name, $request ) {

    $quote = get_field('citaat',$object['id'] );
    return $quote;
}