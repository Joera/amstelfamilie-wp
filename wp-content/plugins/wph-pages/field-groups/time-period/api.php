<?php

add_action( 'init', 'register_time_period' );

function register_time_period() {
    register_rest_field( json_decode(WPH_CONFIG)->field_groups->time_period,
        'time_period',
        array(
            'get_callback'    => 'get_time_period',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_time_period( $object, $field_name, $request ) {
    
    $end_date = get_field('end-date',$object['id']);

    $time_period = new stdClass();
    $time_period->start = get_field('start-date',$object['id']);
    $time_period->end = ($end_date && $end_date != '') ? get_field('end-date',$object['id']) : $time_period->start;
    $time_period->colour = get_field('work_colour',$object['id']);
    $time_period->planning = get_field('work_planning',$object['id']);
    $time_period->list = get_field('work_include_in_list',$object['id']);
    $time_period->optional_title = get_field('work_title_for_planning',$object['id']);

    return $time_period;
}