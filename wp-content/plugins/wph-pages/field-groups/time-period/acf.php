<?php

if(function_exists("register_field_group"))
{
    register_field_group(array (
        'id' => 'acf_extra-velden-post',
        'title' => 'Extra velden post',
        'fields' => array (
            array (
                'key' => 'dates_start777',
                'label' => 'Startdatum',
                'name' => 'start-date',
                'type' => 'date_picker',
                'first_day' => 1,
                'return_format' => 'd/m/Y',
                'display_format' => 'd/m/Y',
                'save_format' => 'yy-mm-dd',
            ),
            array (
                'key' => 'dates_end888',
                'label' => 'Einddatum',
                'name' => 'end-date',
                'type' => 'date_picker',
                'first_day' => 1,
                'return_format' => 'd/m/Y',
                'display_format' => 'd/m/Y',
                'save_format' => 'yy-mm-dd',
            ),
            array (
                'key' => 'acf_work_colour',
                'label' => 'Kleur',
                'name' => 'work_colour',
                'type' => 'radio',
                'choices' => array (
                    'blue' => 'Blauw',
                    'red' => 'Rood',
                    'orange' => 'Oranje',
                    'green' => 'Groen',
                    'milestone' => 'Mijlpaal'
                ),
                'default_value' => 'blue',
                'layout' => 'horizontal'
            ),
            array (
                'key' => 'acf_work_include_in_list',
                'label' => 'Opnemen in overzicht',
                'name' => 'work_include_in_list',
                'type' => 'radio',
                'choices' => array (
                    1 => 'Ja',
                    0 => 'Nee'
                ),
                'default_value' => 'yes',
                'layout' => 'horizontal'
            ),
            array (
                'key' => 'acf_planning',
                'label' => 'Opnemen in planning',
                'name' => 'work_planning',
                'type' => 'radio',
                'choices' => array (
                    'yes' => 'Ja',
                    'no' => 'Nee'
                ),
                'default_value' => 'yes',
                'layout' => 'horizontal'
            ),

            array (
                'key' => 'acf_work_title_for_planning',
                'label' => 'Optionele titel voor planning',
                'name' => 'work_title_for_planning',
                'type' => 'text'
            )
        ),

        'location' => configToLocations('time_period'),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
}

?>