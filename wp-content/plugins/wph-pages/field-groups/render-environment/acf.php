<?php

if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group-render-environments',
        'title' => 'Websites',
        'fields' => array(
            array(
                'key' => 'acf_render-environments',
                'label' => '',
                'name' => 'render-environments',
                'type' => 'checkbox',
                'choices' => array (
                    'amstelveenlijn' => 'Amstelveenlijn',
                    'uithoornlijn' => 'Uithoornlijn',
                    'amsteltram' => 'Amsteltram'
                ),
//                'default_value' => 'right',
                'layout' => 'vertical'
            ),
        ),
        'location' => configToLocations('render_environments'),
        'menu_order' => 0,
        'position' => 'side',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'field',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));

endif;

?>