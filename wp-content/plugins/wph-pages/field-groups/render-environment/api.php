<?php

add_action( 'init', 'register_render_environments' );

function register_render_environments() {
    register_rest_field( json_decode(WPH_CONFIG)->field_groups->render_environments,
        'render_environments',
        array(
            'get_callback'    => 'get_render_environments',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_render_environments( $object, $field_name, $request ) {

    $envs = get_field('render-environments',$object['id'] );
    return $envs;
}