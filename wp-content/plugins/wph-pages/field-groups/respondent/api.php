<?php

add_action( 'init', 'register_contact_subscription_group' );
add_action( 'init', 'register_contact_feedback_group' );

function register_contact_subscription_group() {
    register_rest_field( 'contact',
        'subscription',
        array(
            'get_callback'    => 'get_subscription',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}


function get_subscription( $object, $field_name, $request ) {

    $subscription = new stdClass();
    $subscription->email = get_field('contact_email',$object['id']);
    $subscription->token = get_field('contact_token',$object['id']);
    $subscription->subscription = get_field('contact_subscription',$object['id']);
    $subscription->panel = get_field('contact_panel',$object['id']);
    $subscription->gvb = get_field('contact_gvb',$object['id']);

    return $subscription;
}



function register_contact_feedback_group() {
    register_rest_field( 'contact',
        'panel_data',
        array(
            'get_callback'    => 'get_feedback_group',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_feedback_group( $object, $field_name, $request ) {

    if(get_field('contact_panel',$object['id']) == true) {

        $panel_data = new stdClass();
        $panel_data->salutation = get_field('contact_salutation', $object['id']);
        $panel_data->last_name = get_field('contact_last_name', $object['id']);
        $panel_data->postal_code = get_field('contact_postal_code', $object['id']);
        $panel_data->working_language = get_field('contact_working_language', $object['id']);
        $panel_data->age_group = get_field('contact_age_group', $object['id']);
        $panel_data->interests = get_field('contact_interests', $object['id']);

        return $panel_data;

    } else {
        return null;
    }
}