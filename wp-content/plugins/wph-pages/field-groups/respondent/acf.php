<?php

$languageChoices = array(
	'dutch'	=> 'NL',
	'english'	=> 'EN'
);

$ageChoices = array(
	'under_25'	=> 'Jonger dan 25 jaar',
	'25_49'	=> '25 – 49 jaar',
	'50_75'	=> '50 – 75 jaar',
	'above_75'	=> 'Ouder dan 75 jaar'
);

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_contact_details',
		'title' => 'Extra ',
		'fields' => array (
			array (
				'key' => 'acf_field_contact_email',
				'label' => 'Email',
				'name' => 'contact_email',
				'type' => 'email',
				'placeholder' => 'Email adres',
				'required' => 1,
				'prepend' => '',
				'append' => '',
				'sort_key' => 1,
			),
            array (
                'key' => 'acf_field_contact_token',
                'label' => 'Token',
                'name' => 'contact_token',
                'type' => 'text',
                'placeholder' => 'Token',
                'required' => 1,
                'prepend' => '',
                'append' => '',
            ),
      		array (
				'key' => 'acf_field_contact_subscription',
				'label' => 'Updates per e-mail',
				'name' => 'contact_subscription',
				'type' => 'select',
                'default_value' => 'none',
                'choices' => array(
                    'none'	=> 'Niet',
                    'direct'	=> 'Bij ieder nieuw artikel',
                    'newsletter'	=> 'Alleen de digitale nieuwsbrief'
                )
			),
            array (
                'key' => 'acf_field_permission_gvb',
                'label' => 'GVB meldingen',
                'name' => 'contact_gvb',
                'type' => 'true_false',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '',
                'default_value' => 0,
                'ui' => 0,
                'ui_on_text' => '',
                'ui_off_text' => '',
            ),
            array (
                'key' => 'acf_field_contact_panel',
                'label' => 'Beschikbaar voor tevredenheidsmonitor', // omgevingsmonitor
                'name' => 'contact_panel',
                'type' => 'true_false',
                'default_value' => 'false',
            ),
            array (
                'key' => 'acf_field_provenance',
                'label' => 'Herkomst',
                'name' => 'contact_provenance',
                'type' => 'text',
                'placeholder' => 'Herkomst',
                'required' => 0,
                'prepend' => '',
                'append' => '',
                'default_value' => 'website',
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'acf_field_contact_panel',
                            'operator' => '==',
                            'value' => '1',
                        ),
                    ),
                ),
                'sort_key' => 2,
            ),
            array (
                'key' => 'acf_field_contact_salutation',
                'label' => 'Aanhef',
                'name' => 'contact_salutation',
                'type' => 'select',
                'choices' => array(
                    'heer'	=> 'heer',
                    'mevrouw'	=> 'mevrouw'
				),
                'placeholder' => 'Aanhef',
                'required' => 1,
                'prepend' => '',
                'append' => '',
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'acf_field_contact_panel',
                            'operator' => '==',
                            'value' => '1',
                        ),
                    ),
                ),
                'sort_key' => 3,
            ),
            array (
                'key' => 'acf_field_contact_last_name',
                'label' => 'Achternaam',
                'name' => 'contact_last_name',
                'type' => 'text',
                'placeholder' => 'Achternaam',
                'required' => 1,
                'prepend' => '',
                'append' => '',
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'acf_field_contact_panel',
                            'operator' => '==',
                            'value' => '1',
                        ),
                    ),
                ),
                'sort_key' => 4,
            ),
            array (
                'key' => 'acf_field_postal_code',
                'label' => 'Postcode',
                'name' => 'contact_postal_code',
                'type' => 'text',
                'placeholder' => 'Postcode',
                'required' => 1,
                'prepend' => '',
                'append' => '',
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'acf_field_contact_panel',
                            'operator' => '==',
                            'value' => '1',
                        ),
                    ),
                ),
                'sort_key' => 5,
            ),
            array (
                'key' => 'acf_field_contact_language',
                'label' => 'Taal', // omgevingsmonitor
                'name' => 'contact_working_language',
                'type' => 'select',
                'default_value' => 'dutch',
                'choices' => $languageChoices,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'acf_field_contact_panel',
                            'operator' => '==',
                            'value' => '1',
                        ),
                    ),
                ),
                'sort_key' => 6,
            ),
            array (
                'key' => 'acf_field_contact_age_group',
                'label' => 'Leeftijdsgroep', // omgevingsmonitor
                'name' => 'contact_age_group',
                'type' => 'select',
                'choices' => $ageChoices,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'acf_field_contact_panel',
                            'operator' => '==',
                            'value' => '1',
                        ),
                    ),
                ),
                'sort_key' => 7,
            ),
            array (
                'key' => 'acf_field_contact_interest',
                'label' => 'Soort belanghebbende', // omgevingsmonitor
                'name' => 'contact_interests',
                'type' => 'checkbox',
                'choices' => array(
                    'commuter'	=> 'Forens',
                    'public_transport_user'	=> 'Openbaar vervoer reiziger',
                    'car_owner'	=> 'Automobilist',
                    'cyclist'	=> 'Fietser',
                    'pedestrian'	=> 'Voetganger',
					'amstelveen_resident'	=> 'Omwonende',
					'area_resident'	=> 'Inwoner omliggende gemeente',
                    'company'	=> 'Bedrijf'

                ),
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'acf_field_contact_panel',
                            'operator' => '==',
                            'value' => '1',
                        ),
                    ),
                ),
                'sort_key' => 8,
            )
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'contact'
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
?>
