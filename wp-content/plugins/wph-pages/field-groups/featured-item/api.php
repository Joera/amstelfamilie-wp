<?php

add_action( 'init', 'register_featured_item' );

function register_featured_item() {
    register_rest_field( json_decode(WPH_CONFIG)->field_groups->featured_item,
        'featured_item',
        array(
            'get_callback'    => 'get_featured_item',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_featured_item( $object, $field_name, $request ) {

    if (get_field('featured', $object['id'])) {
        return true;
    } else {
        return false;
    }
}