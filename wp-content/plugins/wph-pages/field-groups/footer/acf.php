<?php

if(function_exists("register_field_group"))
{
    register_field_group(array (
        'id' => 'acf_footer',
        'title' => 'Extra footer velden',
        'fields' => array (
            array (
                'key' => 'acf_footer_contact',
                'label' => 'Contact',
                'name' => 'footer_contact',
                'type' => 'wysiwyg',
                'default_value' => '',
                'toolbar' => 'full',
                'media_upload' => 'yes',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'page',
                    'operator' => '==',
                    'value' => '32680',
                ),
            ),
            array(
                array(
                    'param' => 'page',
                    'operator' => '==',
                    'value' => '32679',
                ),
            ),
            array(
                array(
                    'param' => 'page',
                    'operator' => '==',
                    'value' => '32681',
                ),
            )
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'no_box',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
}

?>