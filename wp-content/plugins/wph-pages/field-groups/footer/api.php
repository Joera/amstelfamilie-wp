<?php

add_action( 'init', 'register_acf_footer' );

function register_acf_footer() {
    register_rest_field( array('amstelveenlijn','uithoornlijn','amsteltram'),
        'extra',
        array(
            'get_callback'    => 'get_footer_fields',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_footer_fields( $object, $field_name, $request ) {

    $fields = new stdClass();

    $fields->contact = get_field('acf_footer_contact',$object['id'] );

    return $fields;
}