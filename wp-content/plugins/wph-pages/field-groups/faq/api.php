<?php

add_action( 'init', 'register_faq' );

function register_faq() {
    register_rest_field( json_decode(WPH_CONFIG)->field_groups->faq,
        'faq',
        array(
            'get_callback'    => 'get_faq',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_faq( $object, $field_name, $request ) {

    $faqs = array();

    if (have_rows('acf_faq_repeater',$object['id'])): // check if the flexible content field has rows of data
        while ( have_rows('acf_faq_repeater', $object['id']) ) : the_row();

            $faq = new stdClass();
            $faq->question = get_sub_field('acf_faq_question');
            $faq->answer = get_sub_field('acf_faq_answer');
            $img = get_sub_field('acf_faq_image');
            $faq->image = create_image_object($img);

            array_push($faqs,$faq);

        endwhile;
    endif;

    return $faqs;
}