<?php
/*
Plugin Name: WP PAGES
Plugin URI: zuidas.nl
Description: extending CORE functionality
Author: Joera
Author URI: zuidas.nl
Version: 0.1
*/

/* Disallow direct access to the plugin file */

	if (basename($_SERVER['PHP_SELF']) == basename (__FILE__)) {
		die('Sorry, but you cannot access this page directly.');
	}


    include 'endpoints/respondent.php';
    include 'endpoints/rsvp.php';

    include 'filters/transform-post-object.php';
    include 'filters/transform-attachment-object.php';
    include 'filters/publish-permissions.php';
    include 'filters/log-post-object.php';

    include 'content-types/amstelveenlijn.php';
    include 'content-types/uithoornlijn.php';
    include 'content-types/amsteltram.php';
//	include 'content-types/interview.php';
	include 'content-types/location.php';
	include 'content-types/work.php';
    include 'content-types/contact.php';
//
//
    include 'field-groups/language/api.php';

	include 'field-groups/featured-item/acf.php';
	include 'field-groups/featured-item/api.php';
//
	include 'field-groups/time-period/acf.php';
	include 'field-groups/time-period/api.php';

    include 'field-groups/updates/acf.php';
    include 'field-groups/updates/api.php';

    include 'field-groups/footer/acf.php';
    include 'field-groups/footer/api.php';

    include 'field-groups/respondent/acf.php';
    include 'field-groups/respondent/api.php';
//
//	//	include 'field-groups/interaction/match_comment_author.php';
//
////	// taxonomies
    include 'taxonomies/location-types/acf.php';
    include 'taxonomies/work-types/acf.php';
	include 'taxonomies/locations/acf.php';
//	include 'taxonomies/documents/acf.php';

    include 'field-groups/render-environment/acf.php';
    include 'field-groups/render-environment/api.php';

    include 'field-groups/faq/acf.php';
    include 'field-groups/faq/api.php';

    include 'wp-admin-tweaks/styling.php';
	include 'wp-admin-tweaks/cors.php';
	include 'wp-admin-tweaks/post-custom-columns.php';

    include 'wp-admin-tweaks/works-custom-columns.php';
    include 'wp-admin-tweaks/wph-permalinks.php';
    include 'wp-admin-tweaks/environment_filter_posts.php';
    include 'wp-admin-tweaks/environment_filter_comments.php';
    include 'wp-admin-tweaks/notify_wendy.php';

  //  include 'services/email_db.php';
 //   include 'services/email_queue.php';
//     include 'services/email_rsvp.php';
    include 'services/email_services.php';
//     include 'services/email_store.php';

  //  include 'maintenance/contact-list-export.php';
