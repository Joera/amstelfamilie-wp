<?php
/*
Register custom taxonomy for construction projects
*/


// register custom taxonomy
function location_types_taxonomy_init() {
    // create a new taxonomy
    register_taxonomy(
        'location-types',
        // 'post',
        array('location'),
        array(
            'label' => __( 'Type locatie ' ),
            'hierarchical' => true,
            'public' => true,
            'hierarchical' => true,
            'capabilities' => array(
                // 'assign_terms' => 'edit_guides',
                'assign_terms' => 'edit_posts',
                // 'edit_terms' => 'publish_guides'
            )
        )
    );
}
add_action( 'init', 'location_types_taxonomy_init' );