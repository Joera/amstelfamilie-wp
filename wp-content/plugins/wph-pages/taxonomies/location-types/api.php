<?php

    $stop_type = false;
    $types = array();

    if ($wp_terms = wp_get_post_terms($object['id'],'location-types')) {

        foreach ($wp_terms as $wp_term) {

            $types[] = $wp_term->slug;
            if($wp_term->parent === 47) {
                $stop_type = $wp_term->slug;
            }
        }
    }

    $taxonomies["location-types"] = $types;

    if ($stop_type) {
        $taxonomies["stop-type"] = $stop_type;
    }
