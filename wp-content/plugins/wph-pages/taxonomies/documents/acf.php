<?php 

function document_taxonomy() {
	// create a new taxonomy
	register_taxonomy(
		'document-tag',
		'attachment',
		array(
			'label' => __( 'Tag' ),
			'rewrite' => array( 'slug' => 'document-tag' )
		)
	);
}

add_action( 'init', 'document_taxonomy' );