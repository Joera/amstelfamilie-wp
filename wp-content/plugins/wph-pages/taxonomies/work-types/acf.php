<?php
/*
Register custom taxonomy for construction projects
*/


// register custom taxonomy
function work_types_taxonomy_init() {
    // create a new taxonomy
    register_taxonomy(
        'work-types',
        // 'post',
        array('work'),
        array(
            'label' => __( 'Type werkzaamheden' ),
            'hierarchical' => true,
            'public' => true,
            'hierarchical' => true,
            'capabilities' => array(
                // 'assign_terms' => 'edit_guides',
                'assign_terms' => 'edit_posts',
                // 'edit_terms' => 'publish_guides'
            )
        )
    );
}
add_action( 'init', 'work_types_taxonomy_init' );
