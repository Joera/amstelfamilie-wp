<?php

$related_locations = wp_get_post_terms($object['id'],'work-types');

$locations = array();

foreach( $related_locations as $p){
    $location = get_term( $p );
    $locations[] = array( 'name' => $location->name, 'slug' => $location->slug, 'id' => $location->term_id );
}

$taxonomies['work-types'] = $locations;
