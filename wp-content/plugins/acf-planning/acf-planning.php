<?php

/*
Plugin Name: Advanced Custom Fields: planning visualisatie input
Plugin URI: PLUGIN_URL
Description: Input field for planning visualisation
Version: 1.0.0
Author: Joera en Tom
Author URI: AUTHOR_URL
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

// exit if accessed directly
if( ! defined( 'ABSPATH' ) ) exit;


// check if class already exists
if( !class_exists('acf_plugin_planning') ) :

class acf_plugin_planning {

	function __construct() {

		$this->settings = array(
			'version'	=> '1.0.0',
			'url'		=> plugin_dir_url( __FILE__ ),
			'path'		=> plugin_dir_path( __FILE__ )
		);

		load_plugin_textdomain( 'acf-planning', false, plugin_basename( dirname( __FILE__ ) ) . '/lang' );

		add_action('acf/include_field_types', 	array($this, 'include_field_types')); // v5
		add_action('acf/register_fields', 		array($this, 'include_field_types')); // v4
	}

	function include_field_types( $version = false ) {

		if( !$version ) $version = 4;
		include_once('fields/acf-planning-v' . $version . '.php');
	}
}

new acf_plugin_planning();


// class_exists check
endif;

?>
