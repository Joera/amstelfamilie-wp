<?php

// exit if accessed directly
if( ! defined( 'ABSPATH' ) ) exit;


// check if class already exists
if( !class_exists('NAMESPACE_acf_field_FIELD_NAME') ) :


    class NAMESPACE_acf_field_FIELD_NAME extends acf_field {

        function __construct( $settings ) {

            $this->name = 'planning';
            $this->label = __('Planning visualisatie data', 'TEXTDOMAIN');
            $this->category = __("Layout",'acf');

            $this->defaults = array(
                'initial_value' => '[]'
            );
            $this->l10n = array(
                'error'	=> __('Error! Please enter a higher value', 'TEXTDOMAIN'),
            );

            $this->settings = $settings;
            parent::__construct();
        }

        function render_field_settings( $field ) {

            $field = array_merge($this->defaults, $field);
            $key = $field['name'];

            acf_render_field_setting( $field, array(
                'type'		=>	'input',
                'name'		=>	'fields['.$key.'][initial_value]',
                'value'		=>	$field['initial_value']
            ));
        }

        function render_field( $field ) {

            $field = array_merge($this->defaults, $field);
            $value = $field['value'];
            if (strpos($field['value'], ':') !== false) { // : is not allowed i url encoded string, so if string contains : it is not url encoded
                $value = urlencode( $field['value'] );
            }

            $e = '';
            $e .= '<div id="' . $field['id'] . 'Container" class="planning-data-container">';
            $e .= '<input type="hidden" id="' . $field['id'] . '"  class="' . $field['class'] . ' planning-hidden-data-input" name="' . $field['name'] . '" value="' . $value . '"/>';
            $e .= '<div class="planning-data"></div>';
            $e .= '<div class="planning-data-controls"><input type="button" class="button planning-data-add-item" value="Add data"></div>';
            $e .= '</div>';

            echo $e;
        }

        function input_admin_enqueue_scripts() {

            // vars
            $url = $this->settings['url'];
            $version = $this->settings['version'];

            // register & include JS
            wp_register_script( 'acf-input-planning', "{$url}assets/js/planning-input.js", array('acf-input'), $version );
            wp_enqueue_script('acf-input-planning');

            // register & include CSS
            wp_register_style( 'acf-input-planning', "{$url}assets/css/input.css", array('acf-input'), $version );
            wp_enqueue_style('acf-input-planning');

            // register jQuery UI stylesheet
            wp_register_style( 'jquery-ui', "{$url}assets/css/jquery-ui.css" );
            wp_enqueue_style('jquery-ui');

        }

        function update_value( $value, $post_id, $field ) {
            return $value;
        }

        function format_value( $value, $post_id, $field ) {

            // bail early if no value
            if( empty($value) ) {
                return $value;
            }
            return $value;
        }
    }

    new NAMESPACE_acf_field_FIELD_NAME( $this->settings );

endif;

?>