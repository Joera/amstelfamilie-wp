(function($){

	// css selector for the hidden input field where the data is stored
	var inputSelector = null;

	function initialize_field( $el ) {
		// get data from hidden input field
		inputSelector = '#' + $el.find('input[type=hidden]').attr('id'); // set css selector for the hidden input field where the data is stored
		console.log(inputSelector);
		console.log(data);
		var data = $(inputSelector).val();

		// init data
		if(data === '') {
			data = []; // if input is blank then set empty array
		} else {
			try {
				data = JSON.parse(decodeURIComponent(decodeURI(data))); // parse data as JSON
				renderDataInput(data); // render the new input GUI
			}
			catch(err) {
				console.log(err);
				data = [];
			}
		}

		// init add button
		initAddButton();
	}





	if( typeof acf.add_action !== 'undefined' ) {

		/*
		*  ready append (ACF5)
		*
		*  These are 2 events which are fired during the page load
		*  ready = on page load similar to $(document).ready()
		*  append = on new DOM elements appended via repeater field
		*
		*  @type	event
		*  @date	20/07/13
		*
		*  @param	$el (jQuery selection) the jQuery element which contains the ACF fields
		*  @return	n/a
		*/

		acf.add_action('ready append', function( $el ){

			// search $el for fields of type 'planning'
			acf.get_fields({ type : 'planning'}, $el).each(function(){

				initialize_field( $(this) );

			});

		});


	} else {


		/*
		*  acf/setup_fields (ACF4)
		*
		*  This event is triggered when ACF adds any new elements to the DOM.
		*
		*  @type	function
		*  @since	1.0.0
		*  @date	01/01/12
		*
		*  @param	event		e: an event object. This can be ignored
		*  @param	Element		postbox: An element which contains the new HTML
		*
		*  @return	n/a
		*/

		$(document).on('acf/setup_fields', function(e, postbox){

			$(postbox).find('.field[data-field_type="planning"]').each(function(){

				initialize_field( $(this) );

			});

		});

	}






	/*****************************************************************
	* Data mutations
	*****************************************************************/

	/*
	Update the planning data and write it to the hidden input field
	*/
	function updateDataItem(index, type, value) {
		var data = $(inputSelector).val();

		// parse json
		try {
			data = JSON.parse(decodeURIComponent(decodeURI(data))); // parse data as JSON

			// update value in data
			for(var i = 0; i < data.length; i++) {
				if(parseInt(i) === parseInt(index)) {
					data[i][type] = value;
				}
			}

			// update hidden input, write data as string into hidden input
			$(inputSelector).val(encodeURIComponent(JSON.stringify(data))); // update hidden input, write data as string into hidden input

		}
		catch(err) {
			// failed parsing json
			console.log(err);
		}
	}


	/*
	Delete the planning data item and write it to the hidden input field
	*/
	function deleteDataItem(index) {
		// var data = $('#acf-field-planning').val();
		var data = $(inputSelector).val();

		// parse json
		try {
			data = JSON.parse(decodeURIComponent(decodeURI(data))); // parse data as JSON
			data.splice(index, 1); // remove item from data
			$(inputSelector).val(encodeURIComponent(JSON.stringify(data))); // update hidden input, write data as string into hidden input
			renderDataInput(data); // render the input grid
		}
		catch(err) {
			// failed parsing json
			console.log(err);
		}
	}



	/*****************************************************************
	* Event handlers
	*****************************************************************/

	function onNameChange() {
		var index = $(this).attr('data-index'),
				type = 'name',
				value = $(this).val(); // .replace(/[^\w\s]/gi, ''); // strip all special characters
		updateDataItem(index, type, value);
	}


	function onPositionChange() {
		var index = $(this).attr('data-index'),
				type = 'position',
				value = $(this).val();
		updateDataItem(index, type, value);
	}


	function onFromChange() {
		var index = $(this).attr('data-index'),
				type = 'from',
				value = $(this).val();
		updateDataItem(index, type, value);
	}


	function onToChange() {
		var index = $(this).attr('data-index'),
				type = 'to',
				value = $(this).val();
		updateDataItem(index, type, value);
	}


	function onClassChange() {
		var index = $(this).attr('data-index'),
				type = 'class',
				value = $(this).val();
		updateDataItem(index, type, value);
	}


	function onDelete() {
		var index = $(this).attr('data-index');
		deleteDataItem(index);
	}


	/*****************************************************************
	* Render
	*****************************************************************/

	/*
	Render the input grid


	$lichtgeel: rgb(255,224,99);
$geel: rgb(255,203,0);
$donkergeel: rgb(248,175,0);
$lichtoranje: rgb(242,145,0);
$oranje: rgb(237,114,3);
$lichtrood: rgb(231,78,15);
$rood:  rgb(229,50,18);
$donkerrood: rgb(209,10,17);
$lichtblauw: rgb(156,198,212);
$blauw: rgb(0,154,200);
$donkerblauw: rgb(0,111,157);
$navy: rgb(0,85,120);
	*/
	function renderDataInput(data) {
		var html = '',
				classOptions = [ // options for the class selector

                    {value: 'blue', text: 'Blauw'},
                    {value: 'darkblue', text: 'Donkerblauw'},
                    {value: 'green', text: 'Groen'},
                    {value: 'yellow', text: 'Geel'},
					{value: 'orange', text: 'Oranje'},
					{value: 'red', text: 'Rood'},
                    {value: 'milestone', text: 'Mijlpaal'}
				];

		// Header row
		if(data.length > 0) {
			html += '<div class="input-row">';
				html += '<div class="input-col"><strong>Name</strong></div>';
				html += '<div class="input-col"><strong>Row</strong></div>';
				html += '<div class="input-col"><strong>From</strong></div>';
				html += '<div class="input-col"><strong>To</strong></div>';
				html += '<div class="input-col"><strong>Color</strong></div>';
				html += '<div class="input-col"></div>';
			html += '</div>';
		}



		// Generate data rows html
		for (var i = 0; i < data.length; i++) {
			html += '<div class="input-row" data-index="' + i + '">';
				html += '<div class="input-col">';
					html += '<input type="text" class="planning-data-input-name" value="' + data[i].name + '" data-index="' + i + '" placeholder="Name" />';
				html += '</div>';
				html += '<div class="input-col">';
					html += '<input type="number" class="planning-data-input-position" value="' + data[i].position + '" data-index="' + i + '" placeholder="Position" />';
				html += '</div>';
				html += '<div class="input-col">';
					html += '<input type="text" class="datepicker planning-data-input-fromdate" value="' + data[i].from + '" data-index="' + i + '" placeholder="From" />';
				html += '</div>';
				html += '<div class="input-col">';
					html += '<input type="text" class="datepicker planning-data-input-todate" value="' + data[i].to + '" data-index="' + i + '" placeholder="To" />';
				html += '</div>';
				html += '<div class="input-col">';
					html += '<select class="planning-data-input-class" data-index="' + i + '">';
						for (var j = 0; j < classOptions.length; j++) {
							var selected = (data[i].class === classOptions[j].value) ? ' selected="selected"' : '';
							html += '<option value="' + classOptions[j].value + '"' + selected + '>' + classOptions[j].text + '</option>';
						}
					html += '</select>';
				html += '</div>';
				html += '<div class="input-col">';
					html += '<input type="button" class="button planning-data-item-remove" value="x" data-index="' + i + '" />';
				html += '</div>';
			html += '</div>';
		}

		// Add generated html to the DOM
		$(inputSelector + 'Container').find('.planning-data').html(html);

		// enable datapickers
		$(inputSelector + 'Container').find('.datepicker').datepicker({dateFormat: 'yy-mm-dd'});
		// initDatePickers(data);

		// Set event handlers
		$(inputSelector + 'Container').find('.planning-data-input-name').change(onNameChange);
		$(inputSelector + 'Container').find('.planning-data-input-position').change(onPositionChange);
		$(inputSelector + 'Container').find('.planning-data-input-fromdate').change(onFromChange);
		$(inputSelector + 'Container').find('.planning-data-input-todate').change(onToChange);
		$(inputSelector + 'Container').find('.planning-data-input-class').change(onClassChange);
		$(inputSelector + 'Container').find('.planning-data-item-remove').click(onDelete);
	}


	/*
	Initalize the add button
	*/
	function initAddButton() {
		// Add event handler for the add new button
		$(inputSelector + 'Container').find('.planning-data-add-item').click(function() {
			// Get data from hidden input field
			var data = $(inputSelector).val();

			// init data
			if(data === '') {
				data = []; // if input is blank then set empty array
			} else {
				try {
					data = JSON.parse(decodeURIComponent(decodeURI(data))); // parse data as JSON
				}
				catch(err) {
					data = [];
				}
			}

			// add new data item row
			var d = new Date()
					year = d.getFullYear(),
					month = ((d.getMonth()+1) < 10) ? '0' + (d.getMonth()+1) : d.getMonth()+1,
					day = (d.getDate() < 10) ? '0' + d.getDate() : d.getDate(),
					dateString = year + '-' + month + '-' + day;
			data.push({"name": "", "description": "", "type": "", "position": 1, "from": dateString, "to": dateString, "class": "default"});

			// set updated data as value of hidden input field
			$(inputSelector).val(encodeURIComponent(JSON.stringify(data)));

			// render the new input GUI
			renderDataInput(data);
		});
	}





/*
	function initDatePickers(data) {

		for (var i = 0; i < data.length; i++) {
				var dateFormat = 'yy-mm-dd';

				$('#planning-data-item-fromdate-' + i).datepicker({
								defaultDate: "+1w",
								dateFormat: dateFormat,
								changeMonth: true
						})
						.on( "change", function() {
								$('#planning-data-item-todate-' + i).datepicker( "option", "minDate", getDate( this ) );
								// onFromChange();
						});

				$('#planning-data-item-todate-' + i).datepicker({
								defaultDate: "+1w",
								dateFormat: dateFormat,
								changeMonth: true
						})
						.on( "change", function() {
								$('#planning-data-item-fromdate-' + i).datepicker( "option", "maxDate", getDate( this ) );
								// onToChange();
						});

				function getDate( element ) {
			      var date;
			      try {
			        	date = $.datepicker.parseDate( dateFormat, element.value );
			      } catch( error ) {
			        	date = null;
			      }
			      return date;
		    }
		}

		// $('.planning-data-input-fromdate').change(onFromChange);
		// $('.planning-data-input-todate').change(onToChange);
	}
*/









})(jQuery);
