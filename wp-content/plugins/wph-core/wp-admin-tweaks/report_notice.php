<?php


    function is_edit_page($new_edit = null){
        global $pagenow;
        //make sure we are on the backend
        if (!is_admin()) return false;

        if($new_edit == "edit")
            return in_array( $pagenow, array( 'post.php',  ) );
        elseif($new_edit == "new") //check for new post page
            return in_array( $pagenow, array( 'post-new.php' ) );
        else //check for either new or edit
            return in_array( $pagenow, array( 'post.php', 'post-new.php' ) );
    }

    function sample_admin_notice__success() {

        global $pagenow;

        $report_services = new Report_Services();

        if (is_admin() && $pagenow == 'post.php') {

            $report = get_transient('_cms_report');

            $post_id = $_GET['post'];

            if (is_edit_page('edit') && $report && $report->primary_id == $post_id) {

                $html = $report_services->createHTMLList($report);

                ?>
                <div class="notice notice-success is-dismissible">
                    <ul>
                        <?php echo $html;  ?>
                    </ul>
                </div>
                <?php
            }
        }
    }

    add_action( 'admin_notices', 'sample_admin_notice__success' );