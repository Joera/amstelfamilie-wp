<?php
add_filter( 'upload_mimes', 'permit_geojson_upload', 1, 1 );
function permit_geojson_upload( $mime_types ) {
    $mime_types['json'] = 'application/json'; // Adding .json extension
    $mime_types['geojson'] = 'application/vnd.geo+json'; // Adding .json extension

    return $mime_types;
}