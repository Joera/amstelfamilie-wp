<?php

// important for media library

//
if ( function_exists( 'add_theme_support' ) ) {

  add_image_size('thumb', 500, 500, true);
  add_image_size('klein', 768);
  add_image_size('medium', 1280);
}
//
//function wpb_image_editor_default_to_gd( $editors ) {
//    $gd_editor = 'WP_Image_Editor_GD';
//    $editors = array_diff( $editors, array( $gd_editor ) );
//    array_unshift( $editors, $gd_editor );
//    return $editors;
//}
//
//add_filter( 'wp_image_editors', 'wpb_image_editor_default_to_gd' );
