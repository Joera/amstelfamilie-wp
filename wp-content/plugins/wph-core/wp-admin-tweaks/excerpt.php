<?php

function sg_excerpt_more( $more ) {
    return ' ...';
}

add_filter( 'excerpt_more', 'sg_excerpt_more' );

add_filter( 'excerpt_length', function($length) {
    return 36;
} );