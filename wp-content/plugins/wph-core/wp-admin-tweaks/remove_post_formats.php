<?php
add_action('after_setup_theme', 'wpse65653_remove_formats', 100);
function wpse65653_remove_formats()
{
   remove_theme_support('post-formats');
}