<?php

add_action( 'init', 'register_cdn_fields' );

function register_cdn_fields($types) {


    register_rest_field( 'attachment',
        'cdn',
        array(
            'get_callback'    => 'get_cdn',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_cdn( $object, $field_name, $request ) {

    PC::debug($object->slug);

    $response = array();

    $response['service'] = 'uploadcare';
    $response['id'] = get_post_meta($object['id'], 'external_id', true);
    $response['url'] = 'https://ucarecdn.com/' . get_post_meta($object['id'], 'external_id', true) . '/' . urlencode($object->slug);

    if (get_post_mime_type( $object['id'] ) ==  'application/pdf' ||  get_post_mime_type( $object['id'] ) == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {

        $response['url'] = $response['url'] . '.pdf';
    }


    return $response;

}