<?php

add_action( 'init', 'register_location_field' );

    function register_location_field() {
        register_rest_field( json_decode(WPH_CONFIG)->field_groups->location,
            'location',
            array(
                'get_callback'    => 'get_location',
                'update_callback' => null,
                'schema'          => null,
            )
        );
    }

        function get_location( $object, $field_name, $request ){

            $google_maps_location = get_field('location',$object['id']);
            $mapbox_location = false;

            $faultyObject = get_field('location_mapbox',$object['id']);

            if($faultyObject) {
                $mapbox_location = array();

                    $mapbox_location['lng'] = $faultyObject['lat'];
                    $mapbox_location['lat'] = $faultyObject['lng'];
            }

            return $mapbox_location ? $mapbox_location : $google_maps_location;
        }

