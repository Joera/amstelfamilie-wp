<?php

add_action( 'init', 'register_interaction' );

function register_interaction() {
    register_rest_field( json_decode(WPH_CONFIG)->field_groups->interaction,
        'interaction',
        array(
            'get_callback'    => 'get_interaction',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function create_comment($wp_comment,$postSlug) {

    $comment = new stdClass();
    $content = apply_filters('comment_text', $wp_comment->comment_content);

    $comment->id = (int) $wp_comment->comment_ID;
    $comment->name = $wp_comment->comment_author;
    $comment->is_editor = false;
    $comment->url = $wp_comment->comment_author_url;
    $comment->date = $wp_comment->comment_date; // date($date_format, strtotime($wp_comment->comment_date));
    $comment->content = $content;
    $comment->parent = (int) $wp_comment->comment_parent;
    $comment->rating = get_comment_meta( $wp_comment->comment_ID, 'rating', true );

    if($wp_comment->user_id) {
        $comment->is_editor = true;
    }

    if($wp_comment->user_id != 0 && $postSlug != 'veelgestelde-vragen') {
        $author = get_userdata($wp_comment->user_id);
        $organisation = get_user_meta($wp_comment->user_id,'organisation');

        if($organisation) {
            $comment->organisation = $organisation[0];
            $comment->name = $author->first_name . ' ' . $author->last_name . ' | ' . $organisation[0];
        } else {
            $comment->name = $author->first_name;
        }
        $comment->thumbnail = new stdClass;
        $thumb = get_field('thumbnail',"user_" . $wp_comment->user_id);

        if($thumb) {
            $comment->thumbnail->external_id = get_post_meta($thumb['ID'], 'external_id', true);
            $comment->thumbnail->filename = $thumb['filename'];
        }
    }

    return $comment;
}

function collect_comments($postId) {

    global $wpdb;
    return $wpdb->get_results($wpdb->prepare("
      SELECT *
      FROM $wpdb->comments
      WHERE comment_post_ID = %d
        AND comment_approved = 1
        AND comment_type = ''
      ORDER BY comment_date
    ", $postId));
}

function get_last_comment_date($comments) {

    if ($comments) {
        return $comments[count($comments)-1]->comment_date;
    } else {
        return null;
    }
}

function nest_comments($comments,$postSlug) {

    $threads = array();
    foreach ($comments as $comment) {

        $comment = create_comment($comment,$postSlug);

        if ($comment->parent == 0) {
            // create thread
            $threads[$comment->id] = array($comment);
        }  else {
            // if has comment_parent push to thread of comment_parent
            if ($comment->parent && isset($threads[$comment->parent]) && is_array($threads[$comment->parent])) {
                array_push($threads[$comment->parent],$comment);
            }
        }
    }

    // reverse chronological order of threads - we need latest comments up
    $threads = array_reverse($threads);
    // reverse order of comments in thread
    foreach ($threads as $key => $thread) {

        $thread_parent = $thread[0];

        if(count($thread) > 1) {

//            for zuidas.nl we reverse order the children
//            $thread_children = array_slice($thread, 1);
//            $reversed_children = array_reverse($thread_children);
//            array_unshift($reversed_children, $thread_parent);
//            $threads[$key] = $reversed_children;

            // now we add prev and next ids to indiv comments
            $threadCount = count($thread);

            foreach ($threads[$key] as $k => &$c) {

                if($k < 1) {
                    $c->prev = false;
                    $c->prev_count = 0;
                } else {
                    $c->prev = $threads[$key][$k - 1]->id;
                    $c->prev_count = $k;
                }

                if($k < (count($threads[$key]) - 1)) {
                  $c->next = $threads[$key][$k + 1]->id;
                    $c->next_count = $threadCount - $k - 1;
                } else {
                    $c->next = false;
                    $c->next_count = 0;
                }

            }


        }
    }

    return $threads;
}

function set_appreciation($postID) {

    if(get_field('positive-count', $postID)) {

        $appreciation = new stdClass();
        $positive_count = get_field('positive-count', $postID);
        $negative_count = get_field('negative-count', $postID);
        $total_count = $positive_count + $negative_count;
        if ($total_count > 0) {
            $percentage = round(($positive_count / $total_count) * 100);
        } else {
            $percentage = 0;
        }
        $appreciation->positive_count = $positive_count;
        $appreciation->negative_count = $negative_count;
        $appreciation->total_count = $total_count;
        $appreciation->percentage = $percentage;

        return $appreciation;

    } else {

        return false;
    }
}

function get_interaction($object, $field_name, $request) {

    $comments = collect_comments($object['id']);
    if($comments) {
        $nested_comments = nest_comments($comments,$object['slug']);
    } else {
        $nested_comments = null;
    }

    $comments_count = wp_count_comments($object['id']);

    return array(
        'appreciation' => set_appreciation($object['id']),
        'comment_count' => $comments_count->total_comments,
        'last_comment_date' => get_last_comment_date($comments),
        'nested_comments' => wph_object_to_array($nested_comments)
    );
}