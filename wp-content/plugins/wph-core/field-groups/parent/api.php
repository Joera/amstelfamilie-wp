<?php

add_action( 'init', 'register_parent' );

function register_parent() {
    register_rest_field( json_decode(WPH_CONFIG)->field_groups->parent,
        'parent',
        array(
            'get_callback'    => 'get_parent',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_parent( $object, $field_name, $request ) {

    $parent = wp_get_post_parent_id( $object['id'] );
    return $parent;
}
