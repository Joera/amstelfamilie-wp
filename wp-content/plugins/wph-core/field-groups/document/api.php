<?php

add_action( 'init', 'register_file_fields' );
add_action( 'init', 'register_file_taxonomies' );

function register_file_fields($types){

    register_rest_field('attachment',
        'file',
        array(
            'get_callback' => 'get_file_fields',
            'update_callback' => null,
            'schema' => null,
        )
    );
}


function register_file_taxonomies($types) {

    register_rest_field( 'attachment',
        'taxonomies',
        array(
            'get_callback'    => 'get_file_taxonomies',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_file_fields( $object, $field_name, $request ) {

    $file =  array();

    $file['filename'] = $object['slug'];
    $file['render_enviroments'] = [];

    if(get_post_meta($object['id'], 'attachment_date', true)) {
        $date = new DateTime(get_post_meta($object['id'], 'attachment_date', true));
        $file['date'] = $date->format('U');// date_format($date, "m/Y");
    }

    $filesize = filesize(get_attached_file($object['id']));
    $filesize = size_format($filesize, 2);
    $file['size'] = $filesize;

    $file['document_history'] = array();

    $historyItems = get_post_meta($object['id'],'document_history',true);

    if (is_array($historyItems)) {
        forEach ($historyItems as $itemid) {
            array_push($file['document_history'], get_post($itemid)->post_name);
        }
    }

    $file['posts'] = array();

    if(class_exists('Find_Posts_Using_Attachment')) {
        $find_posts = new Find_Posts_Using_Attachment;
        $posts = $find_posts->get_posts_by_attachment_id($object['id']);

        forEach( $posts as $postid){

            $post = get_post($postid);

            $file['posts'][] = $post->ID;

            $envs = get_field('render-environments', $post->ID);

            if($envs) {
                forEach ($envs as $env) {
                    $file['render_environments'][] = $env;
                }
            }

            switch ($post->post_type) {

                case 'uithoornlijn':
                $file['render_environments'][] = 'uithoornlijn';
                break;

                case 'amsteltram':
                $file['render_environments'][] = 'amsteltram';
                break;

                case 'amstelveenlijn':
                $file['render_environments'][] = 'amstelveenlijn';
                break;
            }

            if (empty($file['date'])) {
                $postDate = new DateTime($post->post_date);
                $file['date'] = $postDate->format('U');
            }
        }
    }

    if (get_post_meta($object['id'],'current_version',true)) {

        $file['posts'][] = 'versie van ' . get_post_meta($object['id'],'current_version',true);
    }

    if($file['posts']) {
        // get language from first post
        $file['language'] = apply_filters( 'wpml_post_language_details', NULL, $file['posts'][0] ) ;
        $file['language']['code'] = $file['language']['language_code'];
    } else {
        // create empty language object
        $file['language'] = array();
        $file['language']['code'] = 'nl';
    }

    // if item has category english than override language.code
    $types = get_post_meta($object['id'], 'file_document_language', true);
    if($types) {
        foreach ($types as $t) {
            $type = get_term($t, 'document-language');
            if ($type && $type->name == 'english') {
                $file['language']['code'] = 'en';
            }
        }
    }

    return $file;
}

function get_file_taxonomies( $object, $field_name, $request ) {

    $taxonomies = array();

    $taxonomies['themes'] = array();

    $acf_types = get_post_meta($object['id'], 'file-themes', true);

    if($acf_types) {
        foreach ($acf_types as $t) {
            $type = get_term($t, 'category');
            if ($type) {
                $o = array(
                    'name' => $type->name,
                    'slug' => $type->slug
                );
                array_push($taxonomies['themes'], $o);
            }
        }
    }

    $taxonomies['construction-projects'] = array();

    $acf_types = get_post_meta($object['id'], 'file_construction-projects', true);
    if($acf_types) {
        foreach ($acf_types as $t) {
            $type = get_term($t, 'construction-project');
            if ($type) {
                $o = array(
                    'name' => $type->name,
                    'slug' => $type->slug
                );
                array_push($taxonomies['construction-projects'], $o);
            }
        }
    }

    $taxonomies['subareas'] = array();

    // first try with acf (for acf field)
    if($acf_types) {
        $acf_types = get_post_meta($object['id'], 'file_subareas', true);
        foreach ($acf_types as $t) {
            $type = get_term($t, 'subarea');
            if ($type) {
                $o = array(
                    'name' => $type->name,
                    'slug' => $type->slug
                );
                array_push($taxonomies['subareas'], $o);
            }
        }
    }

    $taxonomies['document-types'] = array();

    $acf_types = get_post_meta($object['id'], 'file_document_type', true);

    if($acf_types) {
        foreach ($acf_types as $t) {
            $type = get_term($t, 'document-type');
            if ($type) {
                $o = array(
                    'name' => $type->name,
                    'slug' => $type->slug
                );
                array_push($taxonomies['document-types'], $o);
            }
        }
    }

    return $taxonomies;

}



