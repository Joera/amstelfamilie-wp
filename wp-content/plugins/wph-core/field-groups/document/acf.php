<?php
if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'acf_attachment',  // group_5c9cc2fc09c94
        'title' => 'attachment fields',
        'fields' => array(

            array (
                'key' => 'acf_attachment_date',
                'label' => 'Publicatiedatum',
                'name' => 'attachment_date',
                'type' => 'date_picker',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => 'half',
                    'id' => '',
                ),
                'display_format' => 'd-m-Y  ',
                'return_format' => 'd-m-Y',
                'first_day' => 1,
            )
        ),
        'location' => array(
            array(
                array(
                    'param' => 'attachment',
                    'operator' => '==',
                    'value' => 'application/pdf',
                ),
            ),
            array(
                array(
                    'param' => 'attachment',
                    'operator' => '==',
                    'value' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));

endif;