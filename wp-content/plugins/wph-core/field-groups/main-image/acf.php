<?php

if(function_exists("register_field_group")) {


	register_field_group(array (
		'id' => 'acf_main_image',
		'title' => 'Hoofdafbeelding',
		'fields' => array (
			array(
				'key' => 'acf_main_image',
				'label' => '',
				'name' => 'main_image',
				'type' => 'clone',
				'instructions' => '',
				'required' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'clone' => array(
					0 => 'image_element',
				),
				'display' => 'seamless',
				'layout' => 'block',
				'prefix_label' => 1,
				'prefix_name' => 1,
			),
		),
		'location' => configToLocations('main_image'),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'featured_image',
			),
		),
		'menu_order' => 0,
	));
}
