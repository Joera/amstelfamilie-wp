<?php

add_action( 'init', 'register_main_image' );
//add_action( 'init', 'register_square_image' ); // alternative image if automatic crop is not good enough



function register_main_image($types) {
    register_rest_field( json_decode(WPH_CONFIG)->field_groups->main_image,
        'main_image',
        array(
            'get_callback'    => 'get_main_image',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_main_image( $object, $field_name, $request ) {

    $main_image = get_field('main_image',$object['id']);
    if ($main_image) {

        $img = create_image_object_by_id($main_image['image']['ID']);
        $img->valign = get_field('acf_main_image_alignment');
        $img->alttext = (get_field('main_image_alt-text') === '') ? 'Afbeelding' : get_field('main_image_alt-text');
        $img->credits = get_field('main_image_credits');
        $img->caption = get_field('main_image_caption');

        return $img;

    } else {
        return false;
    }
}

