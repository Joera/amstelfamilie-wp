<?php 

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_extra-redacteur-info',
		'title' => 'Extra redacteur info',
		'fields' => array (
			array (
				'key' => 'field_58d182e4ed35f',
				'label' => 'Organisatie',
				'name' => 'organisation',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_58d1834fed360',
				'label' => 'Pasfoto',
				'name' => 'thumbnail',
				'type' => 'image',
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'ef_user',
					'operator' => '==',
					'value' => 'all',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
