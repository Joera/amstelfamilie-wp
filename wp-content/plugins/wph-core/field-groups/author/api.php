<?php

add_action( 'init', 'register_authors' );

function register_authors() {
    register_rest_field( json_decode(WPH_CONFIG)->field_groups->author,
        'author',
        array(
            'get_callback'    => 'get_author',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_author( $object, $field_name, $request ) {

    $author = new stdClass;
    $author->first_name = get_the_author_meta('first_name', $object['author']);
    $author->last_name = get_the_author_meta('last_name', $object['author']);
    $author->organisation = get_field('organisation',"user_" . $object['author']);
    $author->thumbnail = new stdClass;
    $thumb = get_field('thumbnail',"user_" . $object['author']);
    $author->thumbnail->external_id = get_post_meta($thumb['ID'], 'external_id', true);
    $author->thumbnail->filename = $thumb['filename'];

//
    return $author;
}
