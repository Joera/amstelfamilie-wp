<?php

if( function_exists('acf_add_local_field_group') ):

    $layouts = array();

    if (in_array('paragraph',json_decode(WPH_CONFIG)->sections)) :

        $layouts[] = array(
            'key' => '5a0eed98441b9',
            'name' => 'paragraph',
            'label' => 'Paragraaf',
            'display' => 'block',
            'sub_fields' => array(
                array(
                    'key' => 'field_5a0eeda8441ba',
                    'label' => 'Tekst',
                    'name' => 'text',
                    'type' => 'wysiwyg',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'tabs' => 'all',
                    'toolbar' => 'full',
                    'media_upload' => 1,
                    'delay' => 0,
                ),
            ),
            'min' => '',
            'max' => '',
        );

    endif;

    if (in_array('chapter',json_decode(WPH_CONFIG)->sections)) :

        $layouts[] = array(
            'key' => '5a0eee0d441bb',
            'name' => 'chapter',
            'label' => 'Hoofdstuk',
            'display' => 'block',
            'sub_fields' => array(
                array(
                    'key' => 'field_5a0eee14441bc',
                    'label' => 'Naam',
                    'name' => 'name',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
            ),
            'min' => '',
            'max' => '',
        );

    endif;

    if (in_array('streamer',json_decode(WPH_CONFIG)->sections)) :

        $layouts[] = array(
            'key' => '5a0eee2e441bd',
            'name' => 'streamer',
            'label' => 'Streamer',
            'display' => 'block',
            'sub_fields' => array(
                array(
                    'key' => 'field_5a0eee71441be',
                    'label' => 'Tekst',
                    'name' => 'text',
                    'type' => 'textarea',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'maxlength' => '',
                    'rows' => '',
                    'new_lines' => '',
                ),
            ),
            'min' => '',
            'max' => '',
        );

    endif;

    if (in_array('quote',json_decode(WPH_CONFIG)->sections)) :

        $layouts[] = array(
            'key' => '5a0eee7c441bf',
            'name' => 'quote',
            'label' => 'Quote',
            'display' => 'block',
            'sub_fields' => array(
                array(
                    'key' => 'field_5a0eee8b441c0',
                    'label' => 'Foto',
                    'name' => 'thumb',
                    'type' => 'image',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'return_format' => 'array',
                    'preview_size' => 'thumbnail',
                    'library' => 'all',
                    'min_width' => '',
                    'min_height' => '',
                    'min_size' => '',
                    'max_width' => '',
                    'max_height' => '',
                    'max_size' => '',
                    'mime_types' => '',
                ),
                array(
                    'key' => 'field_5a0eef18441c1',
                    'label' => 'Tekst',
                    'name' => 'text',
                    'type' => 'textarea',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'maxlength' => '',
                    'rows' => '',
                    'new_lines' => '',
                ),
                array(
                    'key' => 'field_5a0eef3b441c2',
                    'label' => 'Naam',
                    'name' => 'name',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5a0eef46441c3',
                    'label' => 'Organisatie',
                    'name' => 'organisation',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
            ),
            'min' => '',
            'max' => '',
        );

    endif;

    if (in_array('images',json_decode(WPH_CONFIG)->sections)) :

    $layouts[] = array(
        'label' => 'Afbeeldingen',
        'name' => 'images',
        'display' => 'block',
        'sub_fields' => array(
            array(
                'key' => 'images_4444',
                'label' => 'Afbeeldingen',
                'name' => 'images',
                'type' => 'repeater',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '50',
                    'class' => '',
                    'id' => '',
                ),
                'collapsed' => '',
                'min' => 1,
                'max' => 2,
                'layout' => 'table',
                'button_label' => 'Afbeelding toevoegen',
                'sub_fields' => array(
                    array(
                        'key' => 'image_clone_001',
                        'label' => '',
                        'name' => 'image_clone',
                        'type' => 'clone',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '60',
                            'class' => '',
                            'id' => '',
                        ),
                        'clone' => array(
                            0 => 'image_element',
                        ),
                        'display' => 'group',
                        'layout' => 'block',
                        'prefix_label' => 0,
                        'prefix_name' => 0,
                    ),
                ),
            ),
            array (
                'key' => 'orientation_00899',
                'label' => 'Orientatie',
                'name' => 'orientation',
                'type' => 'radio',
                'choices' => array (
                    'left' => 'Links',
                    'center' => 'Gecentreerd',
                    'right' => 'Rechts'
                ),
                'default_value' => 'center',
                'layout' => 'horizontal'
            ),
            array (
                'key' => 'images_format',
                'label' => 'Formaat',
                'name' => 'images_format',
                'type' => 'radio',
                'choices' => array (
                    'large' => 'Groter',
                    'small' => 'Klein'
                ),
                'default_value' => 'large',
                'layout' => 'horizontal'
            ),
            array (
                'key' => 'images_background',
                'label' => 'Achtergrond',
                'name' => 'images_background',
                'type' => 'radio',
                'choices' => array (
                    'grey' => 'Grijs',
                    'transparent' => 'Geen'
                ),
                'default_value' => 'grey',
                'layout' => 'horizontal'
            )
        )
    );

    endif;

    if (in_array('imageAndText',json_decode(WPH_CONFIG)->sections)) :

        $layouts[] = array(
            'label' => 'Afbeelding + tekst',
            'name' => 'image_and_text',
            'display' => 'block',
            'sub_fields' => array(
                array(
                    'key' => 'image_and_text_image',
                    'label' => '',
                    'name' => 'image_and_text_image',
                    'type' => 'clone',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '40',
                        'class' => '',
                        'id' => '',
                    ),
                    'clone' => array(
                        0 => 'image_element',
                    ),
                    'display' => 'row',
                    'layout' => 'row',
                    'prefix_label' => 0,
                    'prefix_name' => 0,
                ),
                array(
                    'key' => 'image_and_text_text',
                    'label' => 'Tekst',
                    'name' => 'image_and_text_text',
                    'type' => 'wysiwyg',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '100',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'tabs' => 'all',
                    'toolbar' => 'full',
                    'media_upload' => 1,
                    'delay' => 0,
                ),
                array (
                    'key' => 'image_and_text_orientation',
                    'label' => 'Orientatie',
                    'name' => 'image_and_text_orientation',
                    'type' => 'radio',
                    'choices' => array (
                        'left' => 'Links',
                        'right' => 'Rechts'
                    ),
                    'default_value' => 'left',
                    'layout' => 'horizontal'
                ),
                array (
                    'key' => 'image_and_text_format',
                    'label' => 'Formaat',
                    'name' => 'image_and_text_format',
                    'type' => 'radio',
                    'choices' => array (
                        'large' => 'Groter',
                        'small' => 'Klein'
                    ),
                    'default_value' => 'large',
                    'layout' => 'horizontal'
                )
            )
        );

    endif;

    if (in_array('video',json_decode(WPH_CONFIG)->sections)) :

        $layouts[] = array(
            'key' => '5a0eef5a441c4',
            'name' => 'video',
            'label' => 'Video',
            'display' => 'block',
            'sub_fields' => array(
                array(
                    'key' => 'field_5a0eef6d441c5',
                    'label' => 'Youtube ID',
                    'name' => 'youtube-id',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'acf_video_vimeo',
                    'label' => 'Vimeo',
                    'name' => 'vimeo',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'formatting' => 'html',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'acf_video_iframe',
                    'label' => 'Iframe',
                    'name' => 'iframe',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'formatting' => 'html',
                    'maxlength' => '',
                ),
                array (
                    'key' => 'video_format',
                    'label' => 'Formaat',
                    'name' => 'video_format',
                    'type' => 'radio',
                    'choices' => array (
                        'larger' => 'Groter',
                        'large' => 'Groot',
                        'small' => 'Klein'
                    ),
                    'default_value' => 'small',
                    'layout' => 'horizontal'
                ),
                array (
                    'key' => 'video_orientation',
                    'label' => 'Orientatie',
                    'name' => 'video_orientation',
                    'type' => 'radio',
                    'choices' => array (
                        'horizontal' => 'Liggend',
                        'vertical' => 'Staand'
                    ),
                    'default_value' => 'horizontal',
                    'layout' => 'horizontal'
                ),
            ),
            'min' => '',
            'max' => '',
        );

    endif;

    if (in_array('documents',json_decode(WPH_CONFIG)->sections)) :

        $layouts[] = array(
            'key' => '5a0ef53402e97',
            'name' => 'documents',
            'label' => 'Documenten',
            'display' => 'row',
            'sub_fields' => array(
                array (
                    'key' => 'field_5954415c5ttce',
                    'label' => 'Optionele titel',
                    'name' => 'title',
                    'type' => 'text'
                ),
                array (
                    'key' => 'field_5954415c55ece',
                    'label' => 'Weergave',
                    'name' => 'view',
                    'type' => 'select',
                    'column_width' => '',
                    'choices' => array (
                        'list' => 'Lijst',
                        'row' => 'Rijen',
                    ),
                    'default_value' => 'row',
                    'allow_null' => 1,
                    'multiple' => 0,
                ),
                array(
                    'key' => 'files_54dfd4f3e613f',
                    'label' => 'Bestanden',
                    'name' => 'files',
                    'type' => 'repeater',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'row_min' => '',
                    'row_limit' => '',
                    'layout' => 'row',
                    'button_label' => 'Nieuw bestand',
                    'min' => 0,
                    'max' => 0,
                    'collapsed' => '',
                    'sub_fields' => array(
                        array(
                            'key' => 'documents_clone_001',
                            'label' => '',
                            'name' => 'document_clone',
                            'type' => 'clone',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'clone' => array(
                                0 => 'document_element',
                            ),
                            'display' => 'group',
                            'layout' => 'block',
                            'prefix_label' => 0,
                            'prefix_name' => 0,
                        ),
                    ),
                ),
            ),
            'min' => '',
            'max' => '',
        );

    endif;

    add_filter('extra_acf_layouts','add_acf_layouts',10,1);

    $layouts = apply_filters( 'extra_acf_layouts', $layouts );


    acf_add_local_field_group(array(
        'key' => 'group_5a0c43a2a06dc',
        'title' => 'Secties',
        'fields' => array(
            array(
                'key' => 'field_5a0ee2a795990',
                'label' => 'section',
                'name' => 'section',
                'type' => 'flexible_content',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'layouts' => $layouts,
                'button_label' => 'Nieuwe regel',
                'min' => '',
                'max' => '',
            ),
        ),
        'location' => configToLocations('sections'),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));

endif;

