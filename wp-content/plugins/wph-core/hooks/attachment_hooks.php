<?php

function updateAttachment($id) {

    if (in_array(get_post_mime_type( $id ),json_decode(WPH_CONFIG)->library->mime_types)) {

        $publish_methods = new Publish_Methods();
        $publish_methods->saveAttachment($id);
    }

    else if (get_post_mime_type( $id ) ==  'image/jpeg' || get_post_mime_type( $id ) ==  'image/png' || get_post_mime_type( $id ) ==  'image/gif') {
        // check if change in size
        // sent to uploadcare
        // change external_id
    }
}

function publish_attachment( $value, $post_id, $field  ) {

    //   $old_file = get_field($field['name'],$post_id);
    if (in_array(get_post_mime_type( $post_id ),json_decode(WPH_CONFIG)->library->mime_types)) {

        $publish_methods = new Publish_Methods();
        $publish_methods->saveAttachment($value);
    }

    return $value;
}

add_action( 'edit_attachment','updateAttachment',10,1);
add_action( 'add_attachment','updateAttachment',10,1);
add_filter('acf/update_value/name=file', 'publish_attachment', 10, 3);