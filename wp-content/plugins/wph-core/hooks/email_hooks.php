<?php

// get array of term id's from array of term objects
function getTaxonomyIds($taxonomy_array) {
    $ids = array();
    foreach ( $taxonomy_array as $term ) {
        array_push($ids, $term->term_id);
    }
    return $ids;
}

//function send_new_comment_notification($email,$commentID,$commentPostID) {
//
//    $comment = get_comment($commentID);
//    $post = get_post($commentPostID);
//
//    setlocale(LC_ALL, 'nl_NL'); // set locale for date formatting in dutch
//    $datestring = strftime("%e %B"); // set date string
//    $from = json_decode(WPH_CONFIG)->subscription->from;
//    $text = '';
//    $replyTo = json_decode(WPH_CONFIG)->subscription->replyTo;
//    $subject = json_decode(WPH_CONFIG)->subscription->subjects->new_comment . $post->post_title . '"';
//    $html = '';
//
//    $token = '';
//
//    if ($post->post_type == 'post') {
//        $post_url = WP_HOME . '/blog/' . explode("-", $post->post_date)[0] . '/' . $post->post_name; // set the url of the blog post
//    } else if ($post->post_type == 'construction-project') {
//        $post_url = WP_HOME . '/project/' . $post->post_name;
//    } else {
//        $post_url = WP_HOME . '/' . $post->post_name;
//    }
//
//    $mailgun = new Mailgun_Connector();
//    $mailgun->send_email($from, $email, $subject, $text, $html, '', $replyTo,'');
//
//}


function notify_editors_about_new_comment($emails,$commentID) {

    $comment = get_comment($commentID);
    $postID = $comment->comment_post_ID;
    $post = get_post($postID);
    $envs = get_field('render-environments',$postID);
    $env = $envs[0];

    $from = 'noreply@amstelveenlijn.nl';
    $replyTo = 'website@amstelveenlijn.nl';

    // filter maken in wph_pages
    if (in_array("amstelveenlijn", $envs)) {
        array_push($emails, 'joeramulders@gmail.com','Amstelveenlijn.MET@amsterdam.nl');
        $from = 'noreply@amstelveenlijn.nl';
        $replyTo = 'Amstelveenlijn.MET@amsterdam.nl';
    }

    if (in_array("uithoornlijn", $envs)) {
        array_push($emails, 'joeramulders@gmail.com','info@uithoornlijn.nl');
        $from = 'noreply@uithoornlijn.nl';
        $replyTo = 'info@uithoornlijn.nl';
    }

    if (in_array("amsteltram", $envs)) {
        array_push($emails, 'joeramulders@gmail.com','I.Boxem@vervoerregio.nl');
        $from = 'noreply@amsteltram.nl';
        $replyTo = 'info@amsteltram.nl';
    }

    $emails = array_unique($emails);

    setlocale(LC_ALL, 'nl_NL'); // set locale for date formatting in dutch
    $datestring = strftime("%e %B"); // set date string

    $text = '';

    $subject = 'Er is een reactie geplaatst bij ' . $post->post_title . "'";
    $html = '';
    $token = '';
    $email = '';

    $post_url = get_permalink($postID);

    include(PLUGIN_FOLDER . 'wph-pages/email-templates/comment-notification-for-editors.php');

    foreach ( $emails as $email ) {
        $mailgun = new Mailgun_Connector();
        $mailgun->send_email($from, $email, $subject, $text, $html, '', $replyTo, '');
    }
}

// let op ... dit ding wordt in een queue gezet met cron jobs .. de wp cron staat uit .. maar in crontab cronnen we om de 5 minuten
//function do_direct_mail($post_id) {
//
//    $new_status = get_post_status($post_id);
//
//    if ( $new_status == 'publish') {
//        $ch = curl_init(WP_HOME . '/wp-json/wp/v2/email/?method=direct&postID=' . $post_id);
//        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
//        $response = curl_exec($ch);
//        curl_close($ch);
//    }
//}

function direct_mail_hook($post_id) {

    $type = get_post_type($post_id); // get post type

    if ($type == 'post'){

        $new_status = get_post_status($post_id); // get new status
        $old_status = get_post_meta($post_id, 'old_status', true); // get previous status
       //  $silent_mode = get_post_meta($post_id, 'silent_publish_field', true);

        
        if ( $new_status == 'publish' && $old_status != 'publish') {
            $email_controller = new Email_Controller();
            $email_controller->directMail($post_id);
        }
    }
}

//add_action( 'direct_mail_action','do_direct_mail', 10, 3 );
// add_action('acf/save_post', 'direct_mail_hook', 20, 3);

add_filter( 'comment_moderation_recipients', 'notify_editors_about_new_comment', 11, 2 );
add_filter( 'comment_notification_recipients', 'notify_editors_about_new_comment', 11, 2 );

// add_action( 'new_comment_notification', 'send_new_comment_notification', 10, 3 );
//add_action('rsvp_hook', 'send_rsvp_confirmation');

// add_action( 'direct_batch_hook','do_direct_batch', 10, 3 );
