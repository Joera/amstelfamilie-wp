<?php

class Comments_Hooks
{
    public function init()
    {
        $this->hooks();
    }

    public function hooks()
    {
     //  add_action('deleted_comment', array($this, 'refresh_post')); // happens at delete permanently - no action for sg
        add_action('comment_post', array($this, 'refresh_post'));
        add_action('edit_comment', array($this, 'refresh_post')); // geen actie bij trash
        add_action('trashed_comment', array($this, 'refresh_post'));
        add_action('untrashed_comment', array($this, 'refresh_post'));
    }


    public function parent_has_parent($parent_id){

        $grandfather = get_comment($parent_id);

        if($grandfather == false) {
            return 0;
        } elseif ($grandfather->comment_parent > 0) {
            return $this->parent_has_parent($grandfather->comment_parent);
        } else {
            return $grandfather->comment_ID;
        }
    }

    public function refresh_post($id) {

        $comment = get_comment($id);
        $commentarr = array();
        $commentarr['comment_ID'] = $id;
        $commentarr['comment_parent'] = $this->parent_has_parent($comment->comment_parent);
////           uodate functie werkt niet .. geen idee waarom
////           wp_update_comment($commentarr);
////           dus dan maar direct in de db rammen
//
        global $wpdb;
        $sqlc="update $wpdb->comments set comment_parent= ".$commentarr['comment_parent']." where comment_ID= ".$commentarr['comment_ID'];
        $wpdb->query($sqlc);

        $publish_methods = new Publish_Methods();
        $publish_methods->saveContent($comment->comment_post_ID);
    }

}
$comments_hooks = new Comments_Hooks();
$comments_hooks->init();

