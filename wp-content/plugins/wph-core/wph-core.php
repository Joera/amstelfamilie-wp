<?php
/*
Plugin Name: WP CORE
Plugin URI: publikaan.nl
Description: core wp functionality with config file in wp-pages
Author: Joera Mulders
Author URI: publikaan.nl
Version: 0.1
*/

/* Disallow direct access to the plugin file */

if (basename($_SERVER['PHP_SELF']) == basename (__FILE__)) {
	die('Sorry, but you cannot access this page directly.');
}

$wph_config = file_get_contents(dirname(__FILE__) . "/../../plugins/wph-pages/wph_config.json");
$wph_json = json_decode($wph_config,true);

define('WPH_CONFIG', $wph_config);
define('PLUGIN_FOLDER', dirname( __FILE__ ) . '/../../plugins/');

// tweaking basic admin functionality
include 'wp-admin-tweaks/permit_geojson_upload.php';
include 'wp-admin-tweaks/remove_post_formats.php';
include 'wp-admin-tweaks/image-sizes.php';
include 'wp-admin-tweaks/disable_login_hints.php';
include 'wp-admin-tweaks/disable_gutenberg.php';
// include 'wp-admin-tweaks/google_maps_key.php';
include 'wp-admin-tweaks/report_notice.php';
include 'wp-admin-tweaks/cors.php';
include 'wp-admin-tweaks/excerpt.php';
include 'wp-admin-tweaks/upload_limits.php';
include 'wp-admin-tweaks/mapbox.php';

include 'connectors/mailgun.php';
include 'connectors/mailchimp.php';

include 'services/object-to-array.php';
include 'services/find-posts-using-attachment.php';
include 'services/clean_api_response.php';
include 'services/config-to-locations.php';
include 'services/document-object.php';
include 'services/image-object.php';
include 'services/email-services.php';
include 'services/email-store.php';
include 'services/report-services.php';

// Controllers
include 'controllers/publish_controller.php';
include 'controllers/email_controller.php';

include 'hooks/publish_hooks.php';
include 'hooks/comment_hooks.php';
include 'hooks/preview_hooks.php';
include 'hooks/image_hooks.php';
include 'hooks/attachment_hooks.php';
include 'hooks/email_hooks.php';

include 'endpoints/all-types.php';
include 'endpoints/all-documents.php';
include 'endpoints/submit-comment.php';
include 'endpoints/post-rating.php';
include 'endpoints/comment-rating.php';
include 'endpoints/subscriber.php';
include 'endpoints/subscriptions.php';
include 'endpoints/form.php';
include 'endpoints/mailchimp.php';
include 'endpoints/emails.php';

include 'field-groups/clones/image.php';
include 'field-groups/clones/document.php';

include dirname(__FILE__) . "/../../plugins/wph-pages/field-groups/sections/wph_add_sections.php";
foreach (json_decode(WPH_CONFIG)->available_core_field_groups as &$group) {
    foreach(glob(ABSPATH . 'wp-content/plugins/wph-core/field-groups/' . $group . '/*.php') as $file){
        include $file;
    }
}


include 'taxonomies/api.php';
