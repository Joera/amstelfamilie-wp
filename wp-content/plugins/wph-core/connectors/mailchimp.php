<?php
/*
Mailchimp connector
*/

class Mailchimp_Connector {

  // send the email
  function add_contact($contact) {

      $listid = '1e4c3f5644';
      $apiKey = '46a44b501e10738375c410ac4068da49-us20';
      $server = 'us20';

      $auth = base64_encode( 'user:'.$apiKey );
      $data = array(
          'apikey'        => $apiKey,
          'email_address' => $contact->email,
          'status'        => 'subscribed',
          'merge_fields'  => array(
              'FNAME' => $contact->first_name,
              'LNAME' => $contact->last_name
          )
      );
      $json_data = json_encode($data);

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, 'https://'.$server.'.api.mailchimp.com/3.0/lists/'.$listid.'/members/');
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
          'Authorization: Basic '.$auth));
      curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_TIMEOUT, 10);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
      $response = curl_exec($ch);
      $error = curl_error ( $ch );
      curl_close($ch);

      return json_decode($response);

  }


}




?>
