<?php



class Publish_Methods {

    function update($postObject) {

        // $postObject = apply_filters( 'log-post-object', $postObject);

        $ch = curl_init(API_URL_FRONTEND . '/api/content' );
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Authorization: ' . API_URL_FRONTEND_AUTH_KEY));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postObject));

        $response = curl_exec($ch);
        $error = curl_error($ch);
        $info = curl_getinfo($ch);

        curl_close($ch);

        $this->report($response);
    }

    function delete($postObject) {

        $ch = curl_init(API_URL_FRONTEND . '/api/content' );
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Authorization: ' . API_URL_FRONTEND_AUTH_KEY));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postObject));
        $response = curl_exec($ch);
        $error = curl_error($ch);
        PC::debug($error);
        curl_close($ch);
//
        $this->report($response);
    }

    function report($response) {

        $report = json_decode($response);
//        PC::debug($report);

        set_transient( '_cms_report', $report, 30);

        if ($report->error && count($report->error) > 0) {
            // $mailgun = new Mailgun_Connector();
            // $mailgun->send_email('website@amstelveenlijn.nl', 'joeramulders@gmail.com', 'cms report', print_r($report), print_r($report), '', '');
         //   @wp_mail('joeramulders@gmail.com', 'cms report', print_r($report));
        }

    }

    // triggered on save action
    function saveContent( $post_id ) {

            $post = get_post($post_id);

            $controller = new WP_REST_Posts_Controller($post->post_type);
            $request = new WP_REST_Request();

            $WPResponse = $controller->prepare_item_for_response( $post, $request );
            $postData = apply_filters( 'transform-post-object', $WPResponse->data);
            $permission = apply_filters( 'publish-permissions', $post_id);


            if($permission) {
                $this->update($postData);
            }
    }

    function saveAttachment( $post_id ) {

        $post = get_post($post_id);
        $type = get_post_type($post_id);
        $controller = new WP_REST_Posts_Controller($type);

        $request = new WP_REST_Request();

        $WPResponse = $controller->prepare_item_for_response( $post, $request );
        $postData = apply_filters( 'transform-attachment-object', $WPResponse->data);

        $postData['type'] = 'document';
        $this->update($postData);
    }

    // triggered on trash post ands status equals draft
    function deleteContent($post_id) {

        $post = get_post($post_id);

        $controller = new WP_REST_Posts_Controller($post->post_type);
        $request = new WP_REST_Request();

        $WPResponse = $controller->prepare_item_for_response( $post, $request );
        $postData = apply_filters( 'transform-post-object', $WPResponse->data);

        if ($post->post_type != 'field-group' && $post->post_type != 'acf' && $post->post_type != 'feed' && $post->post_type != 'subscriber' && $post->post_type != 'revision') {
            $this->delete($postData);
        }
    }
}
