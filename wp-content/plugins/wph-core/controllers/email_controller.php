<?php

class Email_Controller {

    function init() {
        $this->subscriber_services = new Subscriber_Services();
    }

    function directMail($postID) {

        // get content
        $post = get_post($_GET['postID']);
        $post_url = get_permalink($post->ID);    // WP_HOME . '/blog/' . explode("-", $post->post_date)[0] . '/' . $post->post_name; // set the url of the blog post
//        $languageObject = apply_filters( 'wpml_post_language_details', NULL, $_GET['postID'] ) ;
//        $lan = $languageObject['language_code'];

        $subject = 'Amstelveenlijn | ' . $post->post_title;
        $introductiontext = 'Dit is jouw update over de Amstelveenlijn.';

//        if($lan == 'en') {
//            $subject = 'Zuidas keeps you up to date | ' . $post->post_title;
//            $introductiontext = 'This is your update about Zuidas.';
//        }

        $main_image = get_field('main_image',$post->ID);

        if( $main_image && $main_image['image'] && $main_image['image']['ID']) {
            $imageObject = create_image_object_by_id($main_image['image']['ID']);
            $image_url = 'https://ucarecdn.com/' . $imageObject->external_id . '/-/scale_crop/560x373/center/' . $imageObject->filename;
        } else {
            $image_url = '';
        }

        $subscribers = $this->subscriber_services->findSubscribersByPost($post->ID,'direct');

        $subscribers = array_reverse($subscribers);

        // loop subscribers
        foreach ($subscribers as $subscriber) {
            $email = get_post_meta($subscriber->ID, 'email')[0]; // get email address of subscriber
            $token = get_post_meta($subscriber->ID, 'token')[0];
            // send newsletter
            setlocale(LC_ALL, 'nl_NL'); // set locale for date formatting in dutch
            $datestring = strftime("%e %B"); // set date string
            $from = 'noreply@wijnemenjemee.nl';
            $text = $post_url;
            $replyTo = 'info@wijnemenjemee.nl';
            $html = '';

            include(PLUGIN_FOLDER . 'wph-pages/email-templates/post-notification.php');

            // to do .. set task en set job // voorlopig dit ff testen op mezelf 

            $mailgun = new Mailgun_Connector();
        //    $mailgun->send_email($from, $email, $subject, $text, $html, '', $replyTo);

        }
    }

    function subscription_confirmation($body, $token) {
        // set current date string
        setlocale(LC_ALL, 'nl_NL');
        $datestring = strftime("%e %B");

        $from = 'Wij nemen je mee <noreply@wijnemenjemee.nl>';
//
//        if($lan == 'en') {
//            $subject = json_decode(WPH_CONFIG)->subscription->subjects->subscription_confirmation_english;
//        } else {
//            $subject = json_decode(WPH_CONFIG)->subscription->subjects->subscription_confirmation;
//        }


        $text = '';
        $replyTo = '';
        $html = '';
        $subject = 'Bevestiging aanmelding tevredenheidsmonitor Amstelveenlijn';

        include(PLUGIN_FOLDER . 'wph-pages/email-templates/subscription_confirmation.php');

        $headers = array('Content-Type: text/html; charset=UTF-8');
        $mailgun = new Mailgun_Connector();
        $mailgun->send_email($from, $body->email, $subject, $text, $html, '',$replyTo);

    }

    function subscription_update($body, $token) {
        // set current date string
        setlocale(LC_ALL, 'nl_NL');
        $datestring = strftime("%e %B");

        $from = 'Amstelveenlijn <noreply@amstelveenlijn.nl>';
//
//        if($lan == 'en') {
//            $subject = json_decode(WPH_CONFIG)->subscription->subjects->subscription_confirmation_english;
//        } else {
//            $subject = json_decode(WPH_CONFIG)->subscription->subjects->subscription_confirmation;
//        }


        $text = '';
        $replyTo = '';
        $html = '';
        $subject =  'Wijzig uw aanmelding tevredenheidsmonitor Amstelveenlijn';

        include(PLUGIN_FOLDER . 'wph-pages/email-templates/subscription_update.php');

        $headers = array('Content-Type: text/html; charset=UTF-8');
        $mailgun = new Mailgun_Connector();
        $response = $mailgun->send_email($from, $body->email, $subject, $text, $html, '',$replyTo);

        return $response;

    }

    function comment_notification($commentID) {
        // set current date string
        setlocale(LC_ALL, 'nl_NL');
        $datestring = strftime("%e %B");

        $comment = get_comment($commentID);

        $post = get_post($comment->comment_post_ID);
        $postAuthor = get_userdata($post->post_author);
        $email = $postAuthor->user_email;

        $postUrl = get_permalink($post->ID);
        $author = $comment->comment_author;

        $from = NULL; $replyTo = NULL; $to = NULL; $cc = NULL;

        if (in_array($post->post_author,[30,43,44])) {

            $from = 'Amstelveenlijn <noreply@amstelveenlijn.nl>';
            $replyTo = 'Amstelveenlijn.MET@amsterdam.nl';
            $to = $email;
            $cc = 'Amstelveenlijn.MET@amsterdam.nl,joeramulders@gmail.com';
            $env = 'amstelveenlijn';

        } elseif(in_array($post->post_author,[45])) {

            $from = 'Uithoornlijn <noreply@uithoornlijn.nl>';
            $replyTo = 'info@uithoornlijn.nl';
            $to = 'info@uithoornlijn.nl';
            $cc = 'joeramulders@gmail.com';
            $env = 'uithoornlijn';

        } elseif(in_array($post->post_author,[46])) {

            $from = 'Amsteltram <noreply@amsteltram.nl>';
            $replyTo = 'info@amsteltram.nl';
            $to = $email;
            $cc = 'joeramulders@gmail.com';
            $env = 'amsteltram';

        }


        $text = '';
        $html = '';
        $subject =  'Nieuwe reactie bij: "' . $post->post_title . '"';

        include(PLUGIN_FOLDER . 'wph-pages/email-templates/new_comment.php');

        $headers = array('Content-Type: text/html; charset=UTF-8');
        $mailgun = new Mailgun_Connector();
        $response = $mailgun->send_email($from, $to, $subject, $text, $html, $cc,$replyTo);

        return $response;

    }

    function newsletter($email, $token, $posts,$introductiontext,$lan) {
        // set locale for dutch date formatting
        setlocale(LC_ALL, 'nl_NL');

        // generate the post html
        $html = '';
        $post_html = ''; // the html of the generated posts content for the newsletter
        $rows_size = 2; // the number of posts per row
        $number_of_posts = count($posts); // total number of posts that will be in the newsletter
        echo 'Aantal berichten: ' . $number_of_posts . '<br>';
        // get posts in batches of the $rows_size
        for ($i = 0; $i <= ceil($number_of_posts/$rows_size) + 1; $i = $i + $rows_size) {  // $i = $i + $rows_size
            $posts_batch = array_slice($posts, $i, $rows_size); // get batch of posts of $rows_size
            $post_html .= '<tr valign="top">';
            foreach ( $posts_batch as $post ) { // loop the post batch
                $date = strtotime($post->post_date);
                $datestring = strftime("%e %B", $date);

                if ( 'activity' == get_post_type($post->ID) ) {
                    $datestring = '<span style="background:#fcf61e;padding:5px 7px;">Evenement</span>';
                }

                // set date string
                $post_url = get_permalink($post->ID); // WP_HOME . '/blog/' . explode("-", $post->post_date)[0] . '/' . $post->post_name; // set the url of the blog pos

                $main_image = get_field('main_image',$post->ID);

                if( $main_image && $main_image['image'] && $main_image['image']['ID']) {
                    $imageObject = create_image_object_by_id($main_image['image']['ID']);
                    $image_url = 'https://ucarecdn.com/' . $imageObject->external_id . '/-/scale_crop/260x173/center/' . $imageObject->filename;

                } else {
                    $image_url = '';
                }

                $post_html .= '
            <td valign="top">
                <table valign="top" border="0" cellpadding="0" cellspacing="0" width="270" height="270">
                    <tr valign="top">
                        <td valign="top" width="270" height="178" style="padding:0px;">
                           <a href="' . $post_url . '" style="color:black;text-decoration:none;">
                                <img src="' . $image_url . '"/>
                           </a>
                        </td>       
                    </tr>
                    <tr>
                        <td valign="top" width="270" style="padding:0px;" >
                                <span style="font-size:13px;line-height:20px;width:250px;color: #000000;">' . $datestring . '</span>
                                <h2 style="padding: 0; margin: 7px 80px 0 0; line-height:1.35;">
                                    <a href="' . $post_url . '" style="color:black;text-decoration:none;">' . $post->post_title . '</a>
                                </h2>
                        </td>          
                    </tr>
                    <tr>
                        <td valign="top" width="270" style="padding: 10px 0px 40px 0px;" >
                            <div style="font-size:13px;line-height:20px;width:250px;color: #000000;">' . $post->post_content . '</div>
                        </td>      
                    </tr>
                </table>
            </td>
          ';
            }
            $post_html .= '</tr>';
        }

        // set mail parameters
        $datestring = strftime("%e %B"); // set current date string
        $from = json_decode(WPH_CONFIG)->subscription->from;
        $subject = 'Zuidas houdt je op de hoogte';
        $text = '';
        $replyTo = json_decode(WPH_CONFIG)->subscription->replyTo;

        include(PLUGIN_FOLDER . 'wp-pages/email-templates/newsletter.php');

        // send email
        $mailgun = new Mailgun_Connector();
        $mailgun->send_email($from, $email, $subject, $text, $html, '', $replyTo);
    }
}
