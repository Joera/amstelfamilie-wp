<?php

    function get_all_documents($request) {
        // rest_ensure_response() wraps the data we want to return into a WP_REST_Response, and ensures it will be properly returned.
        global $post;
        // Get all types except these

        $size = 100;

        $posts = get_posts(array(
            'post_type' => 'attachment',
            'post_mime_type'    => 'application/pdf',
            'posts_per_page' => $size,
            'offset' => $size * intval($request['page'])
        ));

        $controller = new WP_REST_Posts_Controller('attachment');

        $items = array();

        foreach ( $posts as $post ) {
            $data = $controller->prepare_item_for_response( $post, $request );
            $data->data = apply_filters( 'transform-post-object', $data->data);
            $data->data['type'] = 'document';
            $items[] = $controller->prepare_response_for_collection( $data );
        }

        $response =  new WP_REST_Response($items,200);

        $links = array();

        if(count($items) == $size) {
            $links['next'] = array( 'href' => 'https://cms.amsteltram.nl/wp-json/wp/v2/documents?page=' . (intval($request['page']) + 1));
        }

        $response->add_links($links);

        return $response;
    }
    function register_all_documents_route() {
        register_rest_route( 'wp/v2', '/documents', array(
            'methods'  => WP_REST_Server::READABLE,
            'callback' => 'get_all_documents',
        ) );
    }

    add_action( 'rest_api_init', 'register_all_documents_route' );
