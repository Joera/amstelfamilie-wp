<?php
defined( 'ABSPATH' ) or die( 'You can\'t access this file directly!');



class WPH_Emails {
    public function init() {
        $this->hooks();
        $this->email_controller = new Email_Controller();
    }

    public function hooks() {
        add_action('rest_api_init', array($this, 'register_email_endpoint'));
    }

    public function register_email_endpoint() {
        register_rest_route( 'wp/v2', '/email/', array(
            'methods' => 'GET',
            'callback' => array($this, 'submit_email')
        ) );
    }

    public function submit_email($request) {
        $params = $request->get_params();
        nocache_headers();

        switch ($params['method']) {
            case 'direct':
                // check if subscription url parameter is valid
                if(isset($params['postID'])) {

                    $this->email_controller->directMail($params['postID']);
                }
                break;
            case 'weekly':

                if(isset($_GET['batch'])) {
                    $batch = 50 * $params['batch'];
                } else {
                    $batch = 0;
                }

                $subscribers = $this->email_controllers->findSubscribersBySubscription('weekly',$batch);

                foreach ( $subscribers as $subscriber ) {
                    
                    if (is_object($subscriber)) {

                        $lan = get_field('language',$subscriber->ID);

                        if ($lan === null) {
                            $lan = 'nl';
                        }

                        $email = get_post_meta($subscriber->ID, 'email')[0];
                        $token = get_post_meta($subscriber->ID, 'token')[0];
                        $posts = $this->email_controller->getSubscriberPosts($subscriber->ID,'weekly',$lan);

                        $introductiontext = '';

                        if($lan === 'en') {
                            $introductiontext = 'This is your update about Zuidas. Here you will find a selection of last weeks articles.';

                        } else {
                            $introductiontext = 'Dit is jouw update over Zuidas. Hier vind je de artikelen van de afgelopen week, gebaseerd op jouw voorkeuren.';
                        }

                        if (count($posts) > 0) {

                           $this->email_controller->newsletter($email, $token, $posts, $introductiontext,$lan);
                        }
                    }
                }

                break;
            case 'monthly':

                    if(isset($_GET['batch'])) {
                        $batch = 50 * $params['batch'];
                    } else {
                        $batch = 0;
                    }
                    
                    $subscribers = $this->email_controller->findSubscribersBySubscription('monthly',$batch);

                    foreach ( $subscribers as $subscriber ) {

                        if (is_object($subscriber)) {

                            $lan = get_field('language', $subscriber->ID);

                            if ($lan === null) {
                                $lan = 'nl';
                            }

                            $email = get_post_meta($subscriber->ID, 'email')[0];
                            $token = get_post_meta($subscriber->ID, 'token')[0];
                         //   PC::debug($email);
                            $posts = $this->email_controller->getSubscriberPosts($subscriber->ID, 'monthly', $lan);

                            $introductiontext = 'Dit is jouw update over Zuidas. Hier vind je de artikelen van afgelopen maand, gebaseerd op jouw voorkeuren.';

                            if (count($posts) > 0) {

                              $this->email_controller->newsletter($email, $token, $posts, $introductiontext, $lan);
                            }
                        }
                    }

                break;
            case 'comment';

                    // via hook only

                break;
            case 'rsvp';

                    // endpoint to hook

                break;
            case 'contactform';

                    // straight on the endpoint

                break;
            case 'subscriber';

                // straight on the endpoint

                break;
        }

        return array('status' => 200, 'message' => __('Your email task has been submitted.', 'wph-user-comments'));


    }

}

$wph_emails = new WPH_Emails();
$wph_emails->init();