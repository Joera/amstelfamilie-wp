<?php

class WPH_Subscriptions_Endpoint {


    public function init() {
        $this->hooks();
    }

    public function hooks() {
        add_action('rest_api_init', array($this, 'register_get_subscriptions_endpoint'));
    }


    public function register_get_subscriptions_endpoint() {
        register_rest_route( 'wp/v2', '/subscriptions/', array(
            'methods' => 'GET',
            'callback' => array($this, 'get_subscriptions')
        ) );
    }

    public function get_subscriptions($request) {

        $params = $request->get_params();

        // get the themes form wordpress db
        $themeTerms = get_terms( 'category', array(
            'hide_empty' => false,
        ));

        //    let excludeArray = ['perspectief','static','organisatie'];

        // format the theme array
        $theme = array();
        foreach($themeTerms as $term) {
            if($term->slug != 'uncategorized') {
                array_push($theme, $term);
            }
        }

        // get the construction projects form wordpress db
        $constructionProjectTerms = get_terms( 'construction-project', array(
            'hide_empty' => false,
        ));

        // format the construction project array
        $constructionProject = array();
        foreach($constructionProjectTerms as $term) {
            array_push($constructionProject, $term);
        }

        if ($themeTerms && $constructionProjectTerms) {
            $response = array(
                'theme' => $theme,
                'constructionProject' => $constructionProject,
                'language' => ['nederlands','english']
            );
            return new WP_REST_Response($response,200);
        }

    }
}

$wph_subscriptions_endpoint = new WPH_Subscriptions_Endpoint();
$wph_subscriptions_endpoint->init();



