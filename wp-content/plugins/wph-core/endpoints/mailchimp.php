<?php
defined( 'ABSPATH' ) or die( 'You can\'t access this file directly!');



class WPH_Mailchimp {
    public function init() {
        $this->hooks();
    }

    public function hooks() {
        add_action('rest_api_init', array($this, 'register_mailchimp_endpoint'));
    }

    public function register_mailchimp_endpoint() {
        register_rest_route( 'wp/v2', '/add_mailchimp_contact/', array(
            'methods' => 'POST',
            'callback' => array($this, 'add_contact')
        ) );
    }

    public function add_contact($request) {
        $params = $request->get_params();
        nocache_headers();

        $email = $params['email'];

        $mailchimp = new Mailchimp_Connector();
        $response = $mailchimp->add_contact($email);

        return array('status' => 200, 'message' => $response);
    }
}

$wph_mailchimp = new WPH_Mailchimp();
$wph_mailchimp->init();