<?php
defined( 'ABSPATH' ) or die( 'You can\'t access this file directly!');



class WPH_Form {
    public function init() {
        $this->hooks();
    }

    public function hooks() {
        add_action('rest_api_init', array($this, 'register_form_endpoint'));
    }

    public function register_form_endpoint() {
        register_rest_route( 'wp/v2', '/form/', array(
            'methods' => 'POST',
            'callback' => array($this, 'submit_form')
        ) );
    }

    public function submit_form($request) {
        $params = $request->get_params();
      //  $query
        nocache_headers();

        switch ($params['type']) {


            // tevredenheidmonitor gaat via content

            case 'contact':

                $env = $params['env'];
                $subject = 'Bericht via contactformulier ' . ucfirst($params['env']);
                $from = '';
                $to = 'joeramulders@gmail.com';

                switch ($params['env']) {

                    case 'uithoornlijn':

                        $from = 'Uithoornlijn <noreply@uithoornlijn.nl>';
                        $to = 'info@uithoornlijn.nl';

                        break;

                    case 'amsteltram':

                        $from = 'Amsteltram <noreply@amsteltram.nl>';
                        $to = 'I.Boxem@vervoerregio.nl';

                        break;

                    case 'amstelveenlijn':

                        $from = 'Amstelveenlijn <noreply@amstelveenlijn.nl>';
                        $to = 'Amstelveenlijn.MET@amsterdam.nl';

                        break;
                }

                $content = json_decode(file_get_contents('php://input'),true);

                $html = '';

                include(PLUGIN_FOLDER . 'wph-pages/email-templates/contactform.php');

                $headers = array('Content-Type: text/html; charset=UTF-8');
                $mailgun = new Mailgun_Connector();
                $response = $mailgun->send_email($from, $to, $subject, $content, $html,'joeramulders@gmail.com');

                break;


            case 'newsletter':

                switch ($params['env']) {

                    case 'uithoornlijn':

                        $mailchimp = new Mailchimp_Connector();

                        $contact = new stdClass();
                        $contact->email = $params['email'];
                        $contact->first_name = $params['first_name'];
                        $contact->last_name = $params['last_name'];

                        $response = $mailchimp->add_contact($contact);


                    break;

                    case 'amstelveenlijn':

                        $mailgun = new Mailgun_Connector();

                        $content = $params['email'];
                        $from = 'Amstelveenlijn <noreply@amstelveenlijn.nl>';
                        $to = 'Amstelveenlijn.MET@amsterdam.nl'; // '';
                        $cc = 'joeramulders@gmail.com';

                        $response = $mailgun->send_email($from,$to,'aanmelding nieuwsbrief', $content, $content,$cc);


                    break;
                }



                break;

        }

        return array('status' => 200, 'message' => $response);
    }


}

$wph_form = new WPH_Form();
$wph_form->init();
