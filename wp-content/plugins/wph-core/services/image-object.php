<?php

function create_image_object ($image) {

    if ( isset($image) ) {

        $imageObj = null;
        $imageObj = new stdClass;
        $imageObj->external_id = new stdClass;
        $imageObj->filename = new stdClass;
        $imageObj->title = new stdClass;
        $imageObj->caption = new stdClass;
        $imageObj->external_id = get_post_meta($image['image']['ID'], 'external_id', true);
        $imageObj->filename = basename(get_attached_file($image['image']['ID']));
        $imageObj->alttext = $image['alt-text'];
        $imageObj->caption = $image['caption'];
        $imageObj->credits = $image['credits'];
        return $imageObj;
    };
}

function create_image_object_by_id ($id) {

    if ( isset($id) ) {
        $imageObj = null;
        $imageObj = new stdClass;
        $imageObj->external_id = new stdClass;
        $imageObj->filename = new stdClass;
        $imageObj->external_id = get_post_meta($id, 'external_id', true);
        $imageObj->filename = basename(get_attached_file($id));
        return $imageObj;
    };
}