<?php

function wph_object_to_array($data)
{
    if (is_array($data) || is_object($data))
    {
        $result = array();
        foreach ($data as $key => $value)
        {
            $result[$key] = wph_object_to_array($value);
        }
        return $result;
    }
    return $data;
}
