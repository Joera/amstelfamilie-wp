<?php

function configToLocations($field_group) {

    $locations = array();

    foreach (json_decode(WPH_CONFIG)->field_groups->$field_group as &$type) {
        $locations[] = array (
            array (
                'param' => 'post_type',
                'operator' => '==',
                'value' => $type,
                'order_no' => 0,
                'group_no' => 0,
            ),
        );
    }

    return $locations;
}