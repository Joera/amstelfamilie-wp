<?php

function create_document_object ($document,$post) {
    
    if ( isset($document) ) {


        // functionality duplicates with wp-core/field-groups/cdn/api

        $docObj= [];
        $docObj['post'] = $post;
        $docObj['file_id'] = $document['file']['ID'];
        $docObj['file_name'] = $document['file']['title'];

        $date = new DateTime(get_post_meta($document['file']['ID'], 'attachment_date', true));

        if($date->format('c')) {
            $docObj['date'] = $date->format('c');
        } else {
            $docObj['date'] = $post->post_date;
        }

        // || $post->post_date;// date_format($date, "m/Y");

        $docObj['file_cdn_id'] = get_post_meta($document['file']['ID'], 'external_id', true);
        $docObj['file_cdn_url'] = 'https://ucarecdn.com/' . get_post_meta($document['file']['ID'], 'external_id', true) . '/' . urlencode($docObj['file_name'] . '.pdf');
        $docObj['file_description'] = $document['file']['description'];
        $docObj['slug'] = $docObj['file_name'];

        return $docObj;
    };
}