<?php

class Report_Services {

    public function createHTMLList($report) {

        $urlbase = get_home_url();

        $html = '';

        if (count($report->warn) > 0) {

            $html .= '<li><ul><li><b>waarschuwing</b></li>';
            foreach ($report->warn as &$msg) {

                $html .= '<li>';
                $html .= $msg;
                $html .= '</li>';
            }
            $html .= '</li></ul>';
        }

        if (count($report->error) > 0) {

            $html .= '<li><ul><li><b>error</b></li>';
            foreach ($report->error as &$msg) {

                $html .= '<li>';
                $html .= $msg;
                $html .= '</li>';
            }
            $html .= '</li></ul>';
        }

        if (count($report->rendered) > 0) {

            $html .= '<li><ul><li><b>html gecreeerd</b></li>';
            foreach ($report->rendered as &$msg) {

                // parse $msg amstelveenlijn/nieuws/2019/wat-kunt-u-in-2020-verwachten

                $parts = explode("/", $msg);

                switch($parts[0]) {

                    case 'amstelveenlijn':
                    $parts[0] = 'https://amstelveenlijn.nl';
                    break;

                    case 'uithoornlijn':
                    $parts[0] = 'https://uithoornlijn.nl';
                    break;

                    case 'amsteltram':
                    $parts[0] = 'https://amsteltram.nl';
                    break;
                }

                $msg = implode('/',$parts);

                $html .= '<li><a href="' . $msg . '" target="_blank">';
                $html .= $msg;
                $html .= '</a></li>';
            }
            $html .= '</li></ul>';
        }

        if (count($report->deleted) > 0) {

            $html .= '<li><ul><li><b>html verwijderd</b></li>';
            foreach ($report->deleted as &$msg) {

                $html .= '<li>';
                $html .= $msg;
                $html .= '</li>';
            }
            $html .= '</li></ul>';
        }

        if (count($report->searchIndex) > 0) {

            $html .= '<li><ul><li><b>toegevoegd aan zoekindex</b></li>';
            foreach ($report->searchIndex as $key=>&$value) {

                if ($key > 6) {

                    $html .= '<li>';
                    $html .= $value;
                    $html .= '</li>';
                }
            }
            $html .= '</li></ul>';
        }

        if (count($report->removedFromIndex) > 0) {

            $html .= '<li><ul><li><b>verwijderd uit zoekindex</b></li>';
            foreach ($report->removedFromIndex as &$msg) {

                $html .= '<li>';
                $html .= $msg;
                $html .= '</li>';
            }
            $html .= '</li></ul>';
        }

        if (count($report->datasets) > 0) {

            $html .= '<li><ul>';
            foreach ($report->datasets as &$msg) {

                $html .= '<li>';
                $html .= $msg;
                $html .= '</li>';
            }
            $html .= '</li></ul>';
        }

        return $html;
    }

    public function byEmail($report,$postObject) {

        if($report && (count($report->error) > 0 || count($report->warn) > 0)){

            $html = $postObject['id'];
            $html .= '<ul>';
            $html .= $this->createHTMLList($report);
            $html .= '</ul>';

            $mailgun = new Mailgun_Connector();
            $mailgun->send_email('noreply@wijnemenjemee.nl', 'joeramulders@gmail.com', 'cms report', $html, $html, '', '');
        }
    }

}