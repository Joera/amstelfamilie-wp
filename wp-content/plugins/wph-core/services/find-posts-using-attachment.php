<?php
/*
Plugin Name: Find Posts Using Attachment
Plugin URI: http://wptavern.com/the-problem-with-image-attachments-in-wordpress
Description: Allows to find all posts where a particular attachment (image, video, etc.) is used.
Author: Sergey Biryukov
Author URI: http://profiles.wordpress.org/sergeybiryukov/
Version: 1.0
Text Domain: find-posts-using-attachment
*/ 

class Find_Posts_Using_Attachment {

	function __construct() {
//		add_action( 'plugins_loaded',             array( $this, 'load_plugin_textdomain' ) );

		add_filter( 'attachment_fields_to_edit',  array( $this, 'attachment_fields_to_edit' ), 10, 2 );

		add_filter( 'manage_media_columns',       array( $this, 'manage_media_columns' ) );
		add_action( 'manage_media_custom_column', array( $this, 'manage_media_custom_column' ), 10, 2 );
	}

//	function load_plugin_textdomain() {
//		load_plugin_textdomain( 'find-posts-using-attachment' );
//	}

	function get_posts_by_attachment_id( $attachment_id ) {

        $posts = array();

		if ( get_post_mime_type( $attachment_id ) ==  'application/pdf' || get_post_mime_type( $attachment_id ) == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'  ) {

			$media_query = new WP_Query( array(

				'meta_value'     => $attachment_id,
				'post_type'      => 'any',	
				'fields'         => 'ids',
				'no_found_rows'  => true,
				'posts_per_page' => -1,
			) );

			if($media_query->have_posts()) {
                $posts = $media_query->posts;
            }
		}

		return $posts;
	}

	function get_posts_using_attachment( $attachment_id, $context ) {
		$post_ids = $this->get_posts_by_attachment_id( $attachment_id );

		switch ( $context ) {
			case 'column':
				$item_format   = '<strong>%1$s</strong>, %2$s %3$s<br />';
				$output_format = '%s';
				break;
			case 'details':
			default:
				$item_format   = '%1$s %3$s<br />';
				$output_format = '<div style="padding-top: 8px">%s</div>';
				break;
		}

		$output = '';

		foreach ( $post_ids as $post_id ) {
			$post = get_post( $post_id );
			if ( ! $post ) {
				continue;
			}

			$post_title = _draft_or_post_title( $post );
			$post_type  = get_post_type_object( $post->post_type );

			if ( $post_type && $post_type->show_ui && current_user_can( 'edit_post', $post_id ) ) {
				$link = sprintf( '<a href="%s">%s</a>', get_edit_post_link( $post_id ), $post_title );
			} else {
				$link = $post_title;
			}

            $output .=  $link . '<br/>';
		}

		return $output;
	}

	function attachment_fields_to_edit( $form_fields, $attachment ) {




		$form_fields['used_in'] = array(
			'label' => 'Gebruikt in',
			'input' => 'html',
			'html'  => $this->get_posts_using_attachment( $attachment->ID, 'details' ),
		);

		return $form_fields;
	}

	function manage_media_columns( $columns ) {
		$filtered_columns = array();

		foreach ( $columns as $key => $column ) {
			$filtered_columns[ $key ] = $column;

			if ( 'parent' === $key ) {
				$filtered_columns['used_in'] = __( 'Toegevoegd aan:', 'find-posts-using-attachment' );
			}
		}

		return $filtered_columns;
	}

	function manage_media_custom_column( $column_name, $attachment_id ) {
		switch ( $column_name ) {
			case 'used_in':
				echo $this->get_posts_using_attachment( $attachment_id, 'column' );
				break;
		}
	}

}

$find_posts_using_attachment = new Find_Posts_Using_Attachment;
?>