/**
 * Custom Mapbox JS
 */

/**
 * Create the custom map
 *
 * @param id
 * @param center_lat
 * @param center_lng
 * @param address
 * @param zoom
 * @param styles
 * @param enable_nav_control
 * @param enable_marker
 * @param enable_marker_popup
 */

const lichtgrijs = 'rgb(230,230,230)';
const grijs = 'rgb(220,220,220)';
const donkergrijs = 'rgb(100,100,100)';
const zwart = 'rgb(51,51,51)';
const wit = 'rgb(255,255,255)';

const lichtgeel = 'rgb(255,224,99)';
const geel = 'rgb(255,203,0)';
const donkergeel = 'rgb(248,175,0)';
const lichtoranje = 'rgb(242,145,0)';
const oranje = 'rgb(237,114,3)';
const lichtrood = 'rgb(231,78,15)';
const rood =  'rgb(229,50,18)';
const donkerrood = 'rgb(209,10,17)';
const lichtblauw = 'rgb(156,198,212)';
const blauw = 'rgb(0,154,200)';
const donkerblauw = 'rgb(0,111,157)';
const navy = 'rgb(0,85,120)';

function drawLine(map) {

    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://api.mapbox.com/datasets/v1/wijnemenjemee/cjx2un0mm0eoi2or5sh8tc7fe/features?access_token=pk.eyJ1Ijoid2lqbmVtZW5qZW1lZSIsImEiOiJjaWgwZjB4ZGwwMGdza3FseW02MWNxcmttIn0.l-4VI25pfA5GKukRQTXnWA');
    xhr.send();

    xhr.onreadystatechange = function () {
        var DONE = 4;
        var OK = 200;
        if (xhr.readyState === DONE) {
            if (xhr.status === OK) {

                let data = JSON.parse(xhr.response);

                data.features = data.features.filter( (feature) => {
                    // console.log(feature.geometry.type);
                    return feature.geometry.type == 'LineString';
                });

               map.addSource("lines", {
                    "type": "geojson",
                    "data": data
                });

                map.addLayer({
                    "id": "Lijn",
                    "type": "line",
                    "source": "lines",
                    "layout": {
                        "line-join": "miter",
                        "line-cap": "square"
                    },
                    "paint": {
                        "line-color": {
                            property: 'name',
                            type: 'categorical',
                            stops: [
                                ['amstelveenlijn', donkerrood],
                                ['uithoornlijn', geel]
                            ]
                        },
                        "line-width": 8,
                        "line-dasharray": [.5,.5]
                    },
                });
            }
        }
    }



}

function create_map(id, center_lat, center_lng, address, zoom, styles, enable_nav_control, enable_marker, enable_marker_popup) {
    // bail early if Mapbox required JS is not available
    if (typeof mapboxgl === 'undefined') {
        return;
    }

    try {
        // Create the map coordinates using the values from the form or from the user's selected location
        let map = new mapboxgl.Map({
            container: 'map_' + id,
            zoom: (zoom) ? zoom : '16',
            center: [center_lat, center_lng],
            style: 'mapbox://styles/wijnemenjemee/cjx251nik0ass1cp5nhkapnuo'
        });

        map.on('style.load', function () {
            drawLine(map);
        })

        // Add the navigation control if it is set to be enabled ?>
        if (enable_nav_control) {
            let navControl = new mapboxgl.NavigationControl();
            map.addControl(navControl, 'top-left');
        }

        // Add the geocoder to search for places using Mapbox Geocoding API
        map.addControl(new MapboxGeocoder({
            accessToken: mapboxgl.accessToken,
            placeholder: address
        }).on('result', function (geocoder) {
            let field_id = jQuery('#' + id);
            let hidden_input_lat = jQuery('.input-lat', field_id);
            let hidden_input_lng = jQuery('.input-lng', field_id);
            let hidden_input_address = jQuery('.input-address', field_id);
            let hidden_input_zoom = jQuery('.input-zoom', field_id);
            let center_lat = (geocoder.result.center[0]) ? geocoder.result.center[0] : null;
            let center_lng = (geocoder.result.center[1]) ? geocoder.result.center[1] : null;
            let address = (geocoder.result.place_name) ? geocoder.result.place_name : null;

            // Update the hidden elements using the values from the search result
            if (hidden_input_lat.length > 0 && center_lat) {
                // Update center_lat
                hidden_input_lat.val(center_lat);
            }
            if (hidden_input_lng.length > 0 && center_lng) {
                // Update center_lng
                hidden_input_lng.val(center_lng)
            }
            if (hidden_input_address.length > 0) {
                // Update address
                hidden_input_address.val(address)
            }

            // Create the marker on the searched location
            if (center_lat && center_lng && address) {
                create_marker(map, center_lat, center_lng, address, enable_marker, enable_marker_popup,id);
            }
        }));

        // Create the map marker
        create_marker(map, center_lat, center_lng, address, enable_marker, enable_marker_popup,id);
    } catch (error) {
        // Log important error message
        console.log(error.message);
    }
}

/**
 * Create the map marker
 *
 * @param map
 * @param center_lat
 * @param center_lng
 * @param address
 * @param enable_marker
 * @param enable_marker_popup
 */


function create_marker(map, center_lat, center_lng, address, enable_marker, enable_marker_popup,elementID) {
    // Add the marker if it is set to be enabled
    if (enable_marker) {
        // This GeoJSON will be used to determine where the marker will appear on the map
        let geoJSON = {
            type: 'FeatureCollection',
            features: [{
                type: 'Feature',
                geometry: {
                    type: 'Point',
                    coordinates: [center_lat, center_lng]
                },
                properties: {
                    title: 'Mapbox',
                    description: address
                }
            }]
        };

        // Add marker to the location
        geoJSON.features.forEach(function (marker) {
            // Create an HTML element for each feature
            let el = document.createElement('div');
            el.className = 'marker';

            let width = '41';
            let height = '41';

            // Make a marker for the feature and add to the map
            let map_marker = new mapboxgl.Marker(el, {
                anchor: 'bottom',
                draggable: true,
            }).setLngLat(marker.geometry.coordinates);

            // Set the popup if the marker popup is set to be enabled
            if (enable_marker_popup) {
                map_marker.setPopup(new mapboxgl.Popup({offset: 25}) // adds popup
                    .setHTML('<h3>' + marker.properties.title + '</h3><p>' + marker.properties.description + '</p>'));
            }

            function onDragEnd() {

                var lngLat = map_marker.getLngLat();

                let field_id = jQuery('#' + elementID);
                let hidden_input_lat = jQuery('.input-lat', field_id);
                let hidden_input_lng = jQuery('.input-lng', field_id);
                let hidden_input_address = jQuery('.input-address', field_id);
                let hidden_input_zoom = jQuery('.input-zoom', field_id);
                let center_lng = (lngLat['lat']) ? lngLat['lat'] : null; // ja .. echt .....
                let center_lat = (lngLat['lng']) ? lngLat['lng'] : null;


                // Update the hidden elements using the values from the search result
                if (hidden_input_lat.length > 0 && center_lat) {
                    // Update center_lat
                    hidden_input_lat.val(center_lat);
                }
                if (hidden_input_lng.length > 0 && center_lng) {
                    // Update center_lng
                    hidden_input_lng.val(center_lng)
                }
                if (hidden_input_address.length > 0) {
                    console.log('hi');
                    // Update address
                    hidden_input_address.val('')
                }
            }

            // Add the marker to the map
            map_marker.on('dragend', onDragEnd);
            map_marker.addTo(map);

        });
    }
}
