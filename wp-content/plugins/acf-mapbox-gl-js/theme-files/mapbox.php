<?php
/**
 * Template Name: Mapbox GL JS
 * Template Post Type: post, page
 */
?>

<?php get_header(); ?>

<?php $api = apply_filters( 'acf/fields/mapbox/api', array() ); // Apply filter from functions.php ?>
<?php $fields = get_fields(); // Get all ACF fields of the post/page ?>
<?php if ( isset( $api['key'] ) && count( $fields ) > 0 ): ?>
	<?php foreach ( $fields as $name => $location ): ?>
		<?php // Skip if it's not a map field ?>
		<?php if ( ! isset( $location['lat'] ) && ! isset( $location['lng'] ) ): ?>
			<?php continue; ?>
		<?php endif; ?>
		<?php $acf_mapbox_static = false; ?>
		<?php if (isset($location['acf_mapbox_static'])) $acf_mapbox_static = true; ?>

		<?php $name = 'mapbox'; // give your map a name ?>
		<?php // Set each map's width and height attributes if specified ?>
		<style type="text/css">
		#map_<?php echo $name; ?> {
		<?php if (isset($location['width']) && !empty($location['width'])): ?>width: <?php echo $location['width']; ?>px;<?php endif;?>
		<?php if (isset($location['height']) && !empty($location['height'])): ?>height: <?php echo $location['height']; ?>px;<?php endif;?>
			max-width: 100%;
		}
		</style>
		<!-- Map container with unique ID -->
		<div id="map_<?php echo $name; ?>">
		<?php 
			if ($acf_mapbox_static):
				$map_width = (isset($location['width']) && !empty($location['width'])) ? $location['width'] : 1280;
				$map_height = (isset($location['height']) && !empty($location['height'])) ? $location['height'] : 600;
				$zoom = isset($location['zoom']) ? $location['zoom'] : '16';
				$lat = $location['lat'];
				$lng = $location['lng'];
				$style = $location["styles"];
				$token = $api['key'];
				$marker = '';
				if (isset($location['enable_marker']) && $location['enable_marker']) {
					$marker = '/pin-l-20+0c248d(' . $lat . ',' . $lng . ')';
				}
				
				$url = 'https://api.mapbox.com/styles/v1/mapbox/' . $style . '/static' . $marker . '/' . $lat . ',' . $lng . ',' . $zoom . '/' . $map_width . 'x' . $map_height . '?access_token=' . $token;
				echo '<img src="' . $url . '" alt="">';
			endif; ?>
		</div>
		<!-- Check if we don't use static map image -->
		<?php if(!$acf_mapbox_static): ?>
		<script type="text/javascript">
		jQuery( document ).ready(function() {
			if (mapboxgl) {
		    // Set the access token
		    mapboxgl.accessToken = '<?php echo $api['key']; ?>';
		    try {
		        // Create the map coordinates using the values from the form or from the user's selected location
		        var map = new mapboxgl.Map({
		            container: 'map_<?php echo $name; ?>',
		            zoom: <?php echo (isset($location['zoom'])) ? $location['zoom'] : '16'; ?>,
		            center: [<?php echo $location['lat']; ?>, <?php echo $location['lng']; ?>],
		            style: 'mapbox://styles/wijnemenjemee/cjx251nik0ass1cp5nhkapnuo'
		            });
		            <?php // Add the navigation control if it is set to be enabled ?>
		            <?php if (isset($location['enable_nav_control']) && $location['enable_nav_control']): ?>
		                var navControl = new mapboxgl.NavigationControl();
		            map.addControl(navControl, 'top-left');
		            <?php endif; ?>
		            <?php // Add the marker if it is set to be enabled ?>
		            <?php if (isset($location['enable_marker']) && $location['enable_marker']): ?>
		            // This GeoJSON will be used to determine where the markers will appear on the map
		            var geoJSON = {
		            type: 'FeatureCollection',
		                features: [{
		                    type: 'Feature',
		                    geometry: {
		                        type: 'Point',
		                        coordinates: [<?php echo $location['lat']; ?>, <?php echo $location['lng']; ?>]
		                    },
		                    properties: {
		                        title: 'Mapbox',
		                        description: '<?php echo $location['address']; ?>'
		                    }
		                }]
		            };
		            // Add the marker to map
		            geoJSON.features.forEach(function (marker) {
		            // Create an HTML element for each feature
		            var el = document.createElement('div');
		            el.className = 'marker';
		            // Make a marker for the feature and add to the map
		            var mapMarker = new mapboxgl.Marker(el).setLngLat(marker.geometry.coordinates);
		            <?php if (isset($location['enable_marker_popup']) && $location['enable_marker_popup']): ?>
		            // Set the popup if the marker popup is set to be enabled
		            mapMarker.setPopup(new mapboxgl.Popup({offset: 25}) // adds popup
		            .setHTML('<h3>' + marker.properties.title + '</h3><p>' + marker.properties.description + '</p>')); // popup HTML
		            <?php endif; ?>
		            // Add the marker to the map
		            mapMarker.addTo(map);
		        });
		        <?php endif; ?>
		    } catch (error) {
		    // Log important error message
		    console.log(error.message);
		    }
		}
		});						 
		</script>
		<?php else: ?>
		<div style="color: #FF0000; margin: 0 auto; width: 50%; text-align: center;"><?php echo __('Please set the Mapbox access token and make sure to change the ACF field name. For more info, please read the readme.txt file inside the plugin folder.'); ?></div>
		<?php endif; ?>

	<?php endforeach; ?>
<?php else: ?>
    <div style="color: #FF0000; margin: 0 auto; width: 50%; text-align: center;"><?php echo __( 'Please set the Mapbox access token and make sure to change the ACF field name. For more info, please read the readme.txt file inside the plugin folder.' ); ?></div>
<?php endif; ?>

<?php get_footer(); ?>
